# Fitness_app

Application for daily fitness routine.

This application allows user: 

- to create personal account via email or phone number;
- set the user training profile with parameters (age, gender, height, weight, activity level) used for exercise settings;
- start daily practices for different muscle groups depending on the profile;
- modify number of exercises depending on the user feedback;
- save user's progress to the database. 

Stack of technologies used in the application:

- QtDesigner
- Firebase: auth, database 
- Python libraries: PySide6, abc, re, firebase_admin, json, requests, pandas, calendar, datetime


