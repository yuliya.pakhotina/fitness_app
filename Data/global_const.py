class GlobalConst:
    MIN_BMI = 0
    MAX_BMI = 100
    MIN_AGE = 18
    MAX_AGE = 100
    DEFAULT_BMI = 1
    DEFAULT_PROFILE_COEFFICIENT = 0.85

