import sys
from Scripts.Windows.window_controller import WindowController
from PySide6.QtWidgets import QApplication
from Scripts.Requests.firebase_auth import FirebaseInitialization
from Scripts.Windows.initial_window import InitWindow


if __name__ == "__main__":
    app = QApplication(sys.argv)

    FirebaseInitialization()
    WindowController()
    InitWindow()

    app.exec()
    #sys.exit(app.exec())
