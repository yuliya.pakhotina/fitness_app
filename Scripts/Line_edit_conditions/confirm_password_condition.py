from Scripts.Line_edit_conditions.abstract_condition import AbstractCondition


class ConfirmPasswordCondition(AbstractCondition):
    """Confirm password condition"""
    def is_condition_completed(self, password, conf_password):
        """Check coincedence of password and confirm password"""
        if password != conf_password:
            return "The password confirmation does not match."
        return ""
