from abc import abstractmethod


class AbstractCondition:
    """Abstract condition"""
    @property
    @abstractmethod
    def is_condition_completed(self, *args):
        pass
