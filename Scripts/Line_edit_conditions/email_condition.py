from Scripts.Line_edit_conditions.abstract_condition import AbstractCondition
import re


class EmailCondition(AbstractCondition):
    """Email condition"""
    _reg_ex_email = '^[a-z0-9]+[\._-]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

    def is_condition_completed(self, email):
        """Check format of email"""
        if not (re.search(self._reg_ex_email, email)):
            return "Invalid email format."
        return ""
