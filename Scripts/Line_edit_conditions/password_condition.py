from Scripts.Line_edit_conditions.abstract_condition import AbstractCondition


class PasswordCondition(AbstractCondition):
    """Password condition"""
    PASSWORD_MIN_LENGTH = 6

    def is_condition_completed(self, password):
        """Check if password has enough length"""
        if len(password) < self.PASSWORD_MIN_LENGTH:
            return "This password is too short. It must contain at least 6 symbols."
        return ""
