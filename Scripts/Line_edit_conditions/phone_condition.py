from Scripts.Line_edit_conditions.abstract_condition import AbstractCondition
import re


class PhoneCondition(AbstractCondition):
    """Phone condition"""
    _reg_ex_phone = '\+?\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4}'

    def is_condition_completed(self, phone):
        """Check format of phone"""
        if not (re.search(self._reg_ex_phone, phone)):
            return "Invalid phone format."
        return ""
