from Scripts.Line_edit_conditions.abstract_condition import AbstractCondition
import re


class LoginCondition(AbstractCondition):
    """Login condition"""
    _reg_ex_email = '^[a-z0-9]+[\._-]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    _reg_ex_phone = '\+?\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4}'

    def is_condition_completed(self, login):
        """Check format of login"""
        if not (re.search(self._reg_ex_email, login)) and not (re.search(self._reg_ex_phone, login)):
            return "Invalid login format."
        return ""

    def is_email(self, login):
        """Is type of login equal to email"""
        return re.search(self._reg_ex_email, login)

    def is_phone(self, login):
        """Is type of login equal to phone"""
        return re.search(self._reg_ex_phone, login)

