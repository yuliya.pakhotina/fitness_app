from Scripts.Line_edit_conditions.abstract_condition import AbstractCondition


class NameCondition(AbstractCondition):
    """Name condition"""
    NAME_MIN_LENGTH = 1

    def is_condition_completed(self, name):
        """Check if name is empty"""
        if len(name) < self.NAME_MIN_LENGTH:
            return "Name must be a non-empty."
        return ""
