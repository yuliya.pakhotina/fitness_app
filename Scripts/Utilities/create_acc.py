import re
from Scripts.Line_edit_conditions import confirm_password_condition, email_condition, name_condition, \
    password_condition, phone_condition
from Scripts.Windows.window_controller import WindowController
from firebase_admin import auth, exceptions
from Scripts.Windows import sign_in_window
import json


class CreateAcc:
    """Creation of account"""
    _controller = None
    list = []
    _list_of_ex = {}

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()

    def init_list(self, email, name, password, conf_password, phone=None):
        """Create list of conditions to be completed"""
        self.list = [email_condition.EmailCondition().is_condition_completed(email),
                     name_condition.NameCondition().is_condition_completed(name),
                     password_condition.PasswordCondition().is_condition_completed(password),
                     confirm_password_condition.ConfirmPasswordCondition().is_condition_completed(password,
                                                                                                  conf_password)]
        if phone is not None:
            self.list.insert(0, phone_condition.PhoneCondition().is_condition_completed(phone))

    def create_acc_function(self, email, name, password, conf_password, warning_lbl, phone=None):
        """Create account"""
        warning_lbl.setText("")
        self.init_list(email, name, password, conf_password, phone)
        for item in self.list:
            if len(item) != 0:
                warning_lbl.setText(item)
                return
        try:
            if phone:
                user = auth.create_user(phone_number=phone, email=email, display_name=name, password=password)
            else:
                user = auth.create_user(email=email, display_name=name, password=password)
            element = self._controller.contain_in_list(sign_in_window.SignInWindow.__name__)
            if element is not None:
                self._controller.select_window(element)
            else:
                self._controller.select_window(sign_in_window.SignInWindow())
        except exceptions.FirebaseError as e:
            message = e.args[0]
            message = re.sub(r' \(.+\)', '', message)
            warning_lbl.setText(message)






