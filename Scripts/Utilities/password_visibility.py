from PySide6 import QtWidgets
from PySide6.QtGui import QIcon


class PasswordVisibility:
    """Change passwords visibility"""

    def pass_change_visibility(self, pass_visibility_status, btn, pass_le_select):
        """Turn on/turn off password visibility"""
        pass_visibility_status = not pass_visibility_status
        if pass_visibility_status:
            pass_le_select.setEchoMode(QtWidgets.QLineEdit.Normal)
            btn.setIcon(QIcon('UI/Icons/nonvisible_white'))
        else:
            pass_le_select.setEchoMode(QtWidgets.QLineEdit.Password)
            btn.setIcon(QIcon('UI/Icons/visible_white'))
        return pass_visibility_status
