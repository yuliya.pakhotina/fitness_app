import json
from Scripts.Windows import start_window
from firebase_admin import auth
from Scripts.Line_edit_conditions import login_condition, password_condition
from Scripts.Requests import REST_API
from Scripts.Windows.window_controller import WindowController
from Scripts.Requests.firebase_database import FirebaseDatabase
from Scripts.Windows.Profile.gender import Gender
from Scripts.Windows.Profile.date_of_birth import DateOfBirth
from Scripts.Windows.Profile.height import Height
from Scripts.Windows.Profile.weight import Weight
from Scripts.Windows.Profile.activity_level import ActivityLevel
from Scripts.Windows.Profile.exercise_level import ExerciseLevel
from Scripts.Windows.Profile.profile import UserProfile
from Scripts.Windows.Profile.date_of_practice import DateOfPractice
from Scripts.Windows.Profile.exercise_modes import ExerciseModes

class LogIn:
    """Logging in the app"""
    _controller = None
    _db = None
    list = []

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._db = FirebaseDatabase.get_instance()

    def init_list(self, login, password):
        """Create list of conditions to be completed"""
        self.list = [login_condition.LoginCondition().is_condition_completed(login),
                     password_condition.PasswordCondition().is_condition_completed(password)]

    def login_function(self, login, password, warning_lbl):
        """Log in to the app"""
        warning_lbl.setText("")
        self.init_list(login, password)
        for item in self.list:
            if len(item) != 0:
                warning_lbl.setText(item)
                return
        if login_condition.LoginCondition().is_email(login):
            res_req = self.sign_in_with_email_and_password(str(login), password, True)
        elif login_condition.LoginCondition().is_phone(login):
            res_req = self.sign_in_with_phone_and_password(str(login), password, True)
        json_string = json.dumps(res_req)
        if login_condition.LoginCondition().is_email(login):
            if "'error':" in str(res_req):
                error_message = json.dumps(res_req['error']['message'])
                error_message = error_message.strip('"').replace('_', ' ').lower().capitalize() + '.'
                warning_lbl.setText(error_message)
            else:
                user = auth.get_user_by_email(login)
        elif login_condition.LoginCondition().is_phone(login):
            if "'error':" in str(res_req):
                error_message = json.dumps(res_req['error']['message'])
                error_message = error_message.strip('"').replace('_', ' ').lower().capitalize() + '.'
                warning_lbl.setText(error_message)
            else:
                user = auth.get_user_by_phone_number(login)
        json_string = json_string.rstrip("}")+f', "phone_number": "{user.phone_number}"'+"}"
        if "'error':" in str(res_req):
            error_message = json.dumps(res_req['error']['message'])
            error_message = error_message.strip('"').replace('_', ' ').lower().capitalize() + '.'
            warning_lbl.setText(error_message)
        else:
            with open('account_data.json', 'w') as outfile:
                outfile.write(json_string)
        user_data = self._db.get_user_data()
        if user_data:
            with open('profile_data.json', 'w') as outfile:
                json.dump(user_data, outfile, indent=4, sort_keys=True)
        else:
            profile = UserProfile.get_instance()
            gender = Gender.get_instance()
            date_of_birth = DateOfBirth.get_instance()
            height = Height.get_instance()
            weight = Weight.get_instance()
            activity = ActivityLevel.get_instance()
            exercise_level = ExerciseLevel.get_instance()
            date_of_practice = DateOfPractice.get_instance()
            exercise_modes = ExerciseModes.get_instance()
            profile.save_profile()
        element = self._controller.contain_in_list(start_window.StartWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(start_window.StartWindow())

    def sign_in_with_email_and_password(self, email: str, password: str, return_secure_token: bool = True):
        """Sign in with email and password"""
        rest_api = REST_API.RESTAPI()
        return rest_api.sign_in_email(email, password, return_secure_token)

    def sign_in_with_phone_and_password(self, phone: str, password: str, return_secure_token: bool = True):
        """Sign in with phone and password"""
        user = auth.get_user_by_phone_number(phone)
        rest_api = REST_API.RESTAPI()
        return rest_api.sign_in_email(user.email, password, return_secure_token)