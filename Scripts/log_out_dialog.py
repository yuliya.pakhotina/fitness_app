# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'log_out_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.4
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QGridLayout, QHBoxLayout,
    QLabel, QPushButton, QSizePolicy, QSpacerItem,
    QWidget)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(300, 150)
        Dialog.setMinimumSize(QSize(300, 150))
        Dialog.setMaximumSize(QSize(300, 150))
        icon = QIcon()
        icon.addFile(u"Icons/question.svg", QSize(), QIcon.Normal, QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.gridLayoutWidget = QWidget(Dialog)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(-5, 40, 311, 70))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.Confirm_log_out_layout = QGridLayout()
        self.Confirm_log_out_layout.setObjectName(u"Confirm_log_out_layout")
        self.Confirm_log_out_lbl = QLabel(self.gridLayoutWidget)
        self.Confirm_log_out_lbl.setObjectName(u"Confirm_log_out_lbl")
        font = QFont()
        font.setFamilies([u"Dubai Medium"])
        font.setPointSize(10)
        font.setBold(True)
        self.Confirm_log_out_lbl.setFont(font)
        self.Confirm_log_out_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Confirm_log_out_lbl.setAlignment(Qt.AlignCenter)

        self.Confirm_log_out_layout.addWidget(self.Confirm_log_out_lbl, 0, 2, 1, 1)

        self.horizontalSpacer_51 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Confirm_log_out_layout.addItem(self.horizontalSpacer_51, 1, 3, 1, 1)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.Confirm_log_out_btn = QPushButton(self.gridLayoutWidget)
        self.Confirm_log_out_btn.setObjectName(u"Confirm_log_out_btn")
        font1 = QFont()
        font1.setFamilies([u"Dubai"])
        font1.setPointSize(10)
        font1.setBold(True)
        self.Confirm_log_out_btn.setFont(font1)
        self.Confirm_log_out_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Confirm_log_out_btn.setStyleSheet(u"color: #2D3047; background-color: #92B181;min-width: 6em;border-radius: 10px;")

        self.horizontalLayout_5.addWidget(self.Confirm_log_out_btn)

        self.Stop_log_out_btn = QPushButton(self.gridLayoutWidget)
        self.Stop_log_out_btn.setObjectName(u"Stop_log_out_btn")
        self.Stop_log_out_btn.setFont(font1)
        self.Stop_log_out_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Stop_log_out_btn.setStyleSheet(u"color: #2D3047; background-color:#DE8F6E;min-width: 6em;border-radius: 10px;")

        self.horizontalLayout_5.addWidget(self.Stop_log_out_btn)


        self.Confirm_log_out_layout.addLayout(self.horizontalLayout_5, 1, 2, 1, 1)

        self.horizontalSpacer_52 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Confirm_log_out_layout.addItem(self.horizontalSpacer_52, 1, 0, 1, 1)


        self.gridLayout.addLayout(self.Confirm_log_out_layout, 0, 0, 1, 1)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.Confirm_log_out_lbl.setText(QCoreApplication.translate("Dialog", u"Do you really want to log out?", None))
        self.Confirm_log_out_btn.setText(QCoreApplication.translate("Dialog", u"Yes", None))
        self.Stop_log_out_btn.setText(QCoreApplication.translate("Dialog", u"No", None))
    # retranslateUi

