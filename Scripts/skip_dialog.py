# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'skip_dialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.4
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QGridLayout, QHBoxLayout,
    QLabel, QPushButton, QSizePolicy, QSpacerItem,
    QWidget)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(320, 200)
        Dialog.setMinimumSize(QSize(320, 200))
        Dialog.setMaximumSize(QSize(320, 200))
        icon = QIcon()
        icon.addFile(u"Icons/question.svg", QSize(), QIcon.Normal, QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.gridLayoutWidget = QWidget(Dialog)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(9, 20, 301, 143))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_5, 2, 0, 1, 1)

        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_7, 3, 0, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_2, 0, 2, 1, 1)

        self.horizontalSpacer_8 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_8, 3, 2, 1, 1)

        self.label = QLabel(self.gridLayoutWidget)
        self.label.setObjectName(u"label")
        font = QFont()
        font.setFamilies([u"Dubai Medium"])
        font.setPointSize(11)
        self.label.setFont(font)
        self.label.setStyleSheet(u"font-size: 11pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.label.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label, 0, 1, 1, 1)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.Hard_btn = QPushButton(self.gridLayoutWidget)
        self.Hard_btn.setObjectName(u"Hard_btn")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Hard_btn.sizePolicy().hasHeightForWidth())
        self.Hard_btn.setSizePolicy(sizePolicy)
        self.Hard_btn.setMinimumSize(QSize(96, 0))
        self.Hard_btn.setMaximumSize(QSize(250, 16777215))
        self.Hard_btn.setSizeIncrement(QSize(250, 0))
        self.Hard_btn.setBaseSize(QSize(250, 0))
        font1 = QFont()
        font1.setFamilies([u"Dubai"])
        font1.setPointSize(10)
        font1.setBold(True)
        self.Hard_btn.setFont(font1)
        self.Hard_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Hard_btn.setStyleSheet(u"color: #2D3047; background-color:#DE8F6E;min-width: 6em;border-radius: 10px;")
        self.Hard_btn.setAutoDefault(True)

        self.horizontalLayout_5.addWidget(self.Hard_btn)


        self.gridLayout.addLayout(self.horizontalLayout_5, 1, 1, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_3, 1, 0, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer, 0, 0, 1, 1)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_4, 1, 2, 1, 1)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.Easy_btn = QPushButton(self.gridLayoutWidget)
        self.Easy_btn.setObjectName(u"Easy_btn")
        sizePolicy.setHeightForWidth(self.Easy_btn.sizePolicy().hasHeightForWidth())
        self.Easy_btn.setSizePolicy(sizePolicy)
        self.Easy_btn.setMinimumSize(QSize(96, 0))
        self.Easy_btn.setMaximumSize(QSize(250, 16777215))
        self.Easy_btn.setSizeIncrement(QSize(250, 0))
        self.Easy_btn.setBaseSize(QSize(250, 0))
        self.Easy_btn.setFont(font1)
        self.Easy_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Easy_btn.setStyleSheet(u"color: #2D3047; background-color: #DBD56E;min-width: 6em;border-radius: 10px;")

        self.horizontalLayout_6.addWidget(self.Easy_btn)


        self.gridLayout.addLayout(self.horizontalLayout_6, 2, 1, 1, 1)

        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_6, 2, 2, 1, 1)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.Complete_btn = QPushButton(self.gridLayoutWidget)
        self.Complete_btn.setObjectName(u"Complete_btn")
        sizePolicy.setHeightForWidth(self.Complete_btn.sizePolicy().hasHeightForWidth())
        self.Complete_btn.setSizePolicy(sizePolicy)
        self.Complete_btn.setMinimumSize(QSize(96, 0))
        self.Complete_btn.setMaximumSize(QSize(250, 16777215))
        self.Complete_btn.setSizeIncrement(QSize(250, 0))
        self.Complete_btn.setBaseSize(QSize(250, 0))
        self.Complete_btn.setFont(font1)
        self.Complete_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Complete_btn.setStyleSheet(u"color: #2D3047; background-color: #92B181;min-width: 6em;border-radius: 10px;")

        self.horizontalLayout_7.addWidget(self.Complete_btn)


        self.gridLayout.addLayout(self.horizontalLayout_7, 3, 1, 1, 1)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Why do you want to skip this exercise?", None))
        self.Hard_btn.setText(QCoreApplication.translate("Dialog", u"It is too hard", None))
        self.Easy_btn.setText(QCoreApplication.translate("Dialog", u"It is very easy", None))
        self.Complete_btn.setText(QCoreApplication.translate("Dialog", u"I've completed this exercise", None))
    # retranslateUi

