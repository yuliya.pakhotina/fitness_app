from firebase_admin import db
import json
from Scripts.Utilities.singleton_class import Singleton


class FirebaseDatabase(Singleton):
    """Connect internal data with Firebase Fatabase"""
    def upload_db(self):
        """ Upload Profile data to Firebase Database"""
        uid = self.get_uid()
        ref = db.reference('users/' + uid)
        with open("profile_data.json", "r") as file:
            file_contents = json.load(file)
        ref.set(file_contents)

    def get_user_data(self):
        """Get User information from Firebase Database"""
        uid = self.get_uid()
        ref = db.reference('users/' + uid)
        user_data = ref.get()
        if not user_data:
            return None
        return user_data

    def get_uid(self):
        """Get User ID from user account"""
        with open('account_data.json') as inputfile:
            data = json.load(inputfile)
        return data["localId"]

    def upload_exercises(self):
        """Upload set of exercises to Firebase Database"""
        ref = db.reference('exercises')
        with open("Data/Exercises.json", "r") as file:
            file_contents = json.load(file)
        ref.set(file_contents)

    def upload_dates(self):
        """Upload Dates of practice to Firebase Database"""
        ref = db.reference('exercises')
        with open("profile_data.json", "r") as file:
            file_contents = json.load(file)
        ref.set(file_contents)

