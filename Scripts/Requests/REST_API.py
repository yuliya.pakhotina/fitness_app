import json
import requests


class RESTAPI:
    """REST API requests for Requests"""

    FIREBASE_WEB_API_KEY = "AIzaSyCDcXBRjrkTg7fewO92F4ZzPub-rWMTjCw"
    rest_api_url_auth = f"https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword"
    rest_api_url_reset_pass=f"https://identitytoolkit.googleapis.com/v1/accounts:sendOobCode?key={FIREBASE_WEB_API_KEY}"

    def sign_in_email(self, email, password, return_secure_token):
        """Sign in to existing account via email"""
        payload = json.dumps({
            "email": email,
            "password": password,
            "returnSecureToken": return_secure_token
        })
        r = requests.post(self.rest_api_url_auth,
                          params={"key": self.FIREBASE_WEB_API_KEY},
                          data=payload)
        return r.json()

    def reset_password(self, email):
        """Reset password of existing account"""
        payload = json.dumps({
            "requestType": "PASSWORD_RESET",
            "email": email
        })
        r = requests.post(self.rest_api_url_reset_pass,
                          params={"key": self.FIREBASE_WEB_API_KEY},
                          data=payload)
        return r.json()
