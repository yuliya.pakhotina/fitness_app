import firebase_admin
from firebase_admin import credentials


class FirebaseInitialization:
    """Initialization of Requests"""

    def __init__(self):
        """Constructor"""
        cred = credentials.Certificate('authdemo-353eb-firebase-adminsdk-pfwei-f91c79b335.json')
        firebase_admin.initialize_app(cred, {'databaseURL': "https://authdemo-353eb-default-rtdb.firebaseio.com/"})





