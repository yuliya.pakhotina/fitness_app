# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main_window.ui'
##
## Created by: Qt User Interface Compiler version 6.2.4
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractScrollArea, QApplication, QCalendarWidget, QComboBox,
    QDateEdit, QFrame, QGridLayout, QHBoxLayout,
    QLabel, QLayout, QLineEdit, QMainWindow,
    QProgressBar, QPushButton, QScrollArea, QSizePolicy,
    QSlider, QSpacerItem, QSpinBox, QStackedWidget,
    QVBoxLayout, QWidget)
#import icons_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(358, 798)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        icon = QIcon()
        icon.addFile(u":/app_icon/app_icon", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.stackedWidget = QStackedWidget(self.centralwidget)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.stackedWidget.setGeometry(QRect(0, -10, 360, 800))
        self.stackedWidget.setMinimumSize(QSize(360, 800))
        self.stackedWidget.setMaximumSize(QSize(360, 800))
        self.Welcome_screen = QWidget()
        self.Welcome_screen.setObjectName(u"Welcome_screen")
        self.Welcome_screen.setMinimumSize(QSize(360, 800))
        self.Welcome_screen.setMaximumSize(QSize(360, 800))
        self.Welcome_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.horizontalLayoutWidget_8 = QWidget(self.Welcome_screen)
        self.horizontalLayoutWidget_8.setObjectName(u"horizontalLayoutWidget_8")
        self.horizontalLayoutWidget_8.setGeometry(QRect(0, 190, 361, 241))
        self.Welcome_layout = QHBoxLayout(self.horizontalLayoutWidget_8)
        self.Welcome_layout.setObjectName(u"Welcome_layout")
        self.Welcome_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_9 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Welcome_layout.addItem(self.horizontalSpacer_9)

        self.Welcome_label = QLabel(self.horizontalLayoutWidget_8)
        self.Welcome_label.setObjectName(u"Welcome_label")
        self.Welcome_label.setStyleSheet(u"color: #EAE8FF; font-size: 28pt; font-weight: 600;")
        self.Welcome_label.setAlignment(Qt.AlignCenter)
        self.Welcome_label.setWordWrap(True)

        self.Welcome_layout.addWidget(self.Welcome_label)

        self.horizontalSpacer_10 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Welcome_layout.addItem(self.horizontalSpacer_10)

        self.stackedWidget.addWidget(self.Welcome_screen)
        self.Sign_in_screen = QWidget()
        self.Sign_in_screen.setObjectName(u"Sign_in_screen")
        self.Sign_in_screen.setEnabled(True)
        self.Sign_in_screen.setMinimumSize(QSize(360, 800))
        self.Sign_in_screen.setMaximumSize(QSize(360, 800))
        self.Sign_in_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.horizontalLayoutWidget_3 = QWidget(self.Sign_in_screen)
        self.horizontalLayoutWidget_3.setObjectName(u"horizontalLayoutWidget_3")
        self.horizontalLayoutWidget_3.setGeometry(QRect(0, 60, 361, 52))
        self.Login_label_layout = QHBoxLayout(self.horizontalLayoutWidget_3)
        self.Login_label_layout.setObjectName(u"Login_label_layout")
        self.Login_label_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_19 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Login_label_layout.addItem(self.horizontalSpacer_19)

        self.label_10 = QLabel(self.horizontalLayoutWidget_3)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setMinimumSize(QSize(0, 50))
        self.label_10.setMaximumSize(QSize(16777215, 50))
        font = QFont()
        font.setFamilies([u"Dubai"])
        font.setPointSize(28)
        font.setBold(True)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet(u"color: #EAE8FF; font-size: 28pt; font-weight: 600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_10.setAlignment(Qt.AlignCenter)

        self.Login_label_layout.addWidget(self.label_10)

        self.horizontalSpacer_20 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Login_label_layout.addItem(self.horizontalSpacer_20)

        self.gridLayoutWidget_6 = QWidget(self.Sign_in_screen)
        self.gridLayoutWidget_6.setObjectName(u"gridLayoutWidget_6")
        self.gridLayoutWidget_6.setGeometry(QRect(-1, 170, 361, 203))
        self.Login_info_layout = QGridLayout(self.gridLayoutWidget_6)
        self.Login_info_layout.setObjectName(u"Login_info_layout")
        self.Login_info_layout.setVerticalSpacing(12)
        self.Login_info_layout.setContentsMargins(0, 0, 0, 0)
        self.Password_login_le = QLineEdit(self.gridLayoutWidget_6)
        self.Password_login_le.setObjectName(u"Password_login_le")
        font1 = QFont()
        font1.setFamilies([u"Dubai"])
        font1.setPointSize(10)
        font1.setItalic(True)
        self.Password_login_le.setFont(font1)
        self.Password_login_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Password_login_le.setMaxLength(32767)

        self.Login_info_layout.addWidget(self.Password_login_le, 3, 1, 1, 1)

        self.Login_le = QLineEdit(self.gridLayoutWidget_6)
        self.Login_le.setObjectName(u"Login_le")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.Login_le.sizePolicy().hasHeightForWidth())
        self.Login_le.setSizePolicy(sizePolicy1)
        self.Login_le.setMinimumSize(QSize(200, 0))
        self.Login_le.setFont(font1)
        self.Login_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Login_le.setInputMask(u"")
        self.Login_le.setMaxLength(32767)
        self.Login_le.setCursorPosition(0)

        self.Login_info_layout.addWidget(self.Login_le, 1, 1, 1, 1)

        self.horizontalSpacer_21 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Login_info_layout.addItem(self.horizontalSpacer_21, 1, 0, 1, 1)

        self.horizontalSpacer_22 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Login_info_layout.addItem(self.horizontalSpacer_22, 1, 2, 1, 1)

        self.label_11 = QLabel(self.gridLayoutWidget_6)
        self.label_11.setObjectName(u"label_11")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.label_11.sizePolicy().hasHeightForWidth())
        self.label_11.setSizePolicy(sizePolicy2)
        font2 = QFont()
        font2.setFamilies([u"Dubai Medium"])
        font2.setPointSize(10)
        font2.setBold(True)
        font2.setItalic(False)
        self.label_11.setFont(font2)
        self.label_11.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")

        self.Login_info_layout.addWidget(self.label_11, 0, 1, 1, 1)

        self.label_12 = QLabel(self.gridLayoutWidget_6)
        self.label_12.setObjectName(u"label_12")
        sizePolicy2.setHeightForWidth(self.label_12.sizePolicy().hasHeightForWidth())
        self.label_12.setSizePolicy(sizePolicy2)
        self.label_12.setFont(font2)
        self.label_12.setStyleSheet(u"font-size: 10pt; color: #EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_12.setFrameShadow(QFrame.Plain)

        self.Login_info_layout.addWidget(self.label_12, 2, 1, 1, 1)

        self.Pass_visible_login_btn = QPushButton(self.gridLayoutWidget_6)
        self.Pass_visible_login_btn.setObjectName(u"Pass_visible_login_btn")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.Pass_visible_login_btn.sizePolicy().hasHeightForWidth())
        self.Pass_visible_login_btn.setSizePolicy(sizePolicy3)
        self.Pass_visible_login_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Pass_visible_login_btn.setStyleSheet(u"background-color: transparent;	border: none;")
        icon1 = QIcon()
        icon1.addFile(u":/visible/visible_white", QSize(), QIcon.Normal, QIcon.Off)
        self.Pass_visible_login_btn.setIcon(icon1)
        self.Pass_visible_login_btn.setIconSize(QSize(20, 20))
        self.Pass_visible_login_btn.setFlat(True)

        self.Login_info_layout.addWidget(self.Pass_visible_login_btn, 3, 2, 1, 1)

        self.horizontalLayoutWidget_4 = QWidget(self.Sign_in_screen)
        self.horizontalLayoutWidget_4.setObjectName(u"horizontalLayoutWidget_4")
        self.horizontalLayoutWidget_4.setGeometry(QRect(-1, 420, 361, 80))
        self.Login_btn_layout = QHBoxLayout(self.horizontalLayoutWidget_4)
        self.Login_btn_layout.setObjectName(u"Login_btn_layout")
        self.Login_btn_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_23 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Login_btn_layout.addItem(self.horizontalSpacer_23)

        self.Login_btn = QPushButton(self.horizontalLayoutWidget_4)
        self.Login_btn.setObjectName(u"Login_btn")
        font3 = QFont()
        font3.setFamilies([u"Dubai Medium"])
        font3.setPointSize(10)
        font3.setBold(True)
        self.Login_btn.setFont(font3)
        self.Login_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Login_btn.setStyleSheet(u"background-color: #92B181; font-size: 10pt; color: #2D3047; min-width: 6em;border-radius: 10px;")
        self.Login_btn.setFlat(False)

        self.Login_btn_layout.addWidget(self.Login_btn)

        self.horizontalSpacer_24 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Login_btn_layout.addItem(self.horizontalSpacer_24)

        self.gridLayoutWidget_8 = QWidget(self.Sign_in_screen)
        self.gridLayoutWidget_8.setObjectName(u"gridLayoutWidget_8")
        self.gridLayoutWidget_8.setGeometry(QRect(-1, 600, 361, 80))
        self.Create_acc_btn_layout = QGridLayout(self.gridLayoutWidget_8)
        self.Create_acc_btn_layout.setObjectName(u"Create_acc_btn_layout")
        self.Create_acc_btn_layout.setVerticalSpacing(12)
        self.Create_acc_btn_layout.setContentsMargins(0, 0, 0, 0)
        self.label_13 = QLabel(self.gridLayoutWidget_8)
        self.label_13.setObjectName(u"label_13")
        font4 = QFont()
        font4.setFamilies([u"Dubai"])
        font4.setPointSize(9)
        font4.setBold(True)
        font4.setItalic(False)
        self.label_13.setFont(font4)
        self.label_13.setStyleSheet(u"color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.label_13.setAlignment(Qt.AlignCenter)

        self.Create_acc_btn_layout.addWidget(self.label_13, 0, 1, 1, 1)

        self.horizontalSpacer_27 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Create_acc_btn_layout.addItem(self.horizontalSpacer_27, 1, 0, 1, 1)

        self.horizontalSpacer_28 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Create_acc_btn_layout.addItem(self.horizontalSpacer_28, 1, 2, 1, 1)

        self.Create_acc_btn = QPushButton(self.gridLayoutWidget_8)
        self.Create_acc_btn.setObjectName(u"Create_acc_btn")
        font5 = QFont()
        font5.setFamilies([u"Dubai"])
        font5.setPointSize(10)
        font5.setBold(True)
        self.Create_acc_btn.setFont(font5)
        self.Create_acc_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Create_acc_btn.setStyleSheet(u"color: #2D3047; background-color:#DBD56E;min-width: 6em;border-radius: 10px;")

        self.Create_acc_btn_layout.addWidget(self.Create_acc_btn, 1, 1, 1, 1)

        self.horizontalLayoutWidget_5 = QWidget(self.Sign_in_screen)
        self.horizontalLayoutWidget_5.setObjectName(u"horizontalLayoutWidget_5")
        self.horizontalLayoutWidget_5.setGeometry(QRect(0, 380, 361, 27))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget_5)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.Warning_login_lbl = QLabel(self.horizontalLayoutWidget_5)
        self.Warning_login_lbl.setObjectName(u"Warning_login_lbl")
        sizePolicy2.setHeightForWidth(self.Warning_login_lbl.sizePolicy().hasHeightForWidth())
        self.Warning_login_lbl.setSizePolicy(sizePolicy2)
        font6 = QFont()
        font6.setFamilies([u"Dubai Medium"])
        font6.setPointSize(9)
        font6.setItalic(False)
        self.Warning_login_lbl.setFont(font6)
        self.Warning_login_lbl.setStyleSheet(u"color: #DE8F6E")
        self.Warning_login_lbl.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.Warning_login_lbl)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)

        self.horizontalLayoutWidget_11 = QWidget(self.Sign_in_screen)
        self.horizontalLayoutWidget_11.setObjectName(u"horizontalLayoutWidget_11")
        self.horizontalLayoutWidget_11.setGeometry(QRect(0, 540, 356, 36))
        self.horizontalLayout_3 = QHBoxLayout(self.horizontalLayoutWidget_11)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_25 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_25)

        self.Reset_pass_btn = QPushButton(self.horizontalLayoutWidget_11)
        self.Reset_pass_btn.setObjectName(u"Reset_pass_btn")
        font7 = QFont()
        font7.setFamilies([u"Dubai"])
        font7.setPointSize(9)
        font7.setBold(True)
        font7.setUnderline(True)
        self.Reset_pass_btn.setFont(font7)
        self.Reset_pass_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Reset_pass_btn.setStyleSheet(u"color: #EAE8FF")
        self.Reset_pass_btn.setFlat(True)

        self.horizontalLayout_3.addWidget(self.Reset_pass_btn)

        self.horizontalSpacer_26 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_26)

        self.stackedWidget.addWidget(self.Sign_in_screen)
        self.Reset_password_screen = QWidget()
        self.Reset_password_screen.setObjectName(u"Reset_password_screen")
        self.Reset_password_screen.setMinimumSize(QSize(360, 800))
        self.Reset_password_screen.setMaximumSize(QSize(360, 800))
        self.Reset_password_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.horizontalLayoutWidget_12 = QWidget(self.Reset_password_screen)
        self.horizontalLayoutWidget_12.setObjectName(u"horizontalLayoutWidget_12")
        self.horizontalLayoutWidget_12.setGeometry(QRect(0, 60, 361, 52))
        self.Reset_pass_lbl_layout = QHBoxLayout(self.horizontalLayoutWidget_12)
        self.Reset_pass_lbl_layout.setObjectName(u"Reset_pass_lbl_layout")
        self.Reset_pass_lbl_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_35 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Reset_pass_lbl_layout.addItem(self.horizontalSpacer_35)

        self.Change_pass_lbl = QLabel(self.horizontalLayoutWidget_12)
        self.Change_pass_lbl.setObjectName(u"Change_pass_lbl")
        self.Change_pass_lbl.setMinimumSize(QSize(0, 50))
        self.Change_pass_lbl.setMaximumSize(QSize(16777215, 50))
        self.Change_pass_lbl.setFont(font)
        self.Change_pass_lbl.setStyleSheet(u"color: #EAE8FF; font-size: 28pt; font-weight: 600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.Change_pass_lbl.setAlignment(Qt.AlignCenter)

        self.Reset_pass_lbl_layout.addWidget(self.Change_pass_lbl)

        self.horizontalSpacer_36 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Reset_pass_lbl_layout.addItem(self.horizontalSpacer_36)

        self.gridLayoutWidget_3 = QWidget(self.Reset_password_screen)
        self.gridLayoutWidget_3.setObjectName(u"gridLayoutWidget_3")
        self.gridLayoutWidget_3.setGeometry(QRect(0, 170, 361, 91))
        self.Reset_pass_layout = QGridLayout(self.gridLayoutWidget_3)
        self.Reset_pass_layout.setObjectName(u"Reset_pass_layout")
        self.Reset_pass_layout.setVerticalSpacing(12)
        self.Reset_pass_layout.setContentsMargins(0, 0, 0, 0)
        self.Login_for_reset_pass_le = QLineEdit(self.gridLayoutWidget_3)
        self.Login_for_reset_pass_le.setObjectName(u"Login_for_reset_pass_le")
        sizePolicy1.setHeightForWidth(self.Login_for_reset_pass_le.sizePolicy().hasHeightForWidth())
        self.Login_for_reset_pass_le.setSizePolicy(sizePolicy1)
        self.Login_for_reset_pass_le.setMinimumSize(QSize(200, 0))
        self.Login_for_reset_pass_le.setFont(font1)
        self.Login_for_reset_pass_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Login_for_reset_pass_le.setInputMask(u"")
        self.Login_for_reset_pass_le.setMaxLength(32767)
        self.Login_for_reset_pass_le.setCursorPosition(0)

        self.Reset_pass_layout.addWidget(self.Login_for_reset_pass_le, 1, 1, 1, 1)

        self.horizontalSpacer_39 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Reset_pass_layout.addItem(self.horizontalSpacer_39, 1, 0, 1, 1)

        self.label_16 = QLabel(self.gridLayoutWidget_3)
        self.label_16.setObjectName(u"label_16")
        sizePolicy2.setHeightForWidth(self.label_16.sizePolicy().hasHeightForWidth())
        self.label_16.setSizePolicy(sizePolicy2)
        self.label_16.setFont(font2)
        self.label_16.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")

        self.Reset_pass_layout.addWidget(self.label_16, 0, 1, 1, 1)

        self.horizontalSpacer_40 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Reset_pass_layout.addItem(self.horizontalSpacer_40, 1, 2, 1, 1)

        self.horizontalLayoutWidget_13 = QWidget(self.Reset_password_screen)
        self.horizontalLayoutWidget_13.setObjectName(u"horizontalLayoutWidget_13")
        self.horizontalLayoutWidget_13.setGeometry(QRect(0, 360, 361, 80))
        self.Reset_pass_btn_layout = QHBoxLayout(self.horizontalLayoutWidget_13)
        self.Reset_pass_btn_layout.setObjectName(u"Reset_pass_btn_layout")
        self.Reset_pass_btn_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_41 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Reset_pass_btn_layout.addItem(self.horizontalSpacer_41)

        self.Confirm_reset_pass_btn = QPushButton(self.horizontalLayoutWidget_13)
        self.Confirm_reset_pass_btn.setObjectName(u"Confirm_reset_pass_btn")
        self.Confirm_reset_pass_btn.setFont(font3)
        self.Confirm_reset_pass_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Confirm_reset_pass_btn.setStyleSheet(u"background-color: #92B181; font-size: 10pt; color: #2D3047; min-width: 6em;border-radius: 10px;")
        self.Confirm_reset_pass_btn.setFlat(False)

        self.Reset_pass_btn_layout.addWidget(self.Confirm_reset_pass_btn)

        self.horizontalSpacer_42 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Reset_pass_btn_layout.addItem(self.horizontalSpacer_42)

        self.horizontalLayoutWidget_14 = QWidget(self.Reset_password_screen)
        self.horizontalLayoutWidget_14.setObjectName(u"horizontalLayoutWidget_14")
        self.horizontalLayoutWidget_14.setGeometry(QRect(0, 270, 361, 27))
        self.Reset_pass_warning_layout = QHBoxLayout(self.horizontalLayoutWidget_14)
        self.Reset_pass_warning_layout.setObjectName(u"Reset_pass_warning_layout")
        self.Reset_pass_warning_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_43 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Reset_pass_warning_layout.addItem(self.horizontalSpacer_43)

        self.Reset_pass_warning_lbl = QLabel(self.horizontalLayoutWidget_14)
        self.Reset_pass_warning_lbl.setObjectName(u"Reset_pass_warning_lbl")
        sizePolicy2.setHeightForWidth(self.Reset_pass_warning_lbl.sizePolicy().hasHeightForWidth())
        self.Reset_pass_warning_lbl.setSizePolicy(sizePolicy2)
        self.Reset_pass_warning_lbl.setFont(font6)
        self.Reset_pass_warning_lbl.setStyleSheet(u"color: #DE8F6E")
        self.Reset_pass_warning_lbl.setAlignment(Qt.AlignCenter)

        self.Reset_pass_warning_layout.addWidget(self.Reset_pass_warning_lbl)

        self.horizontalSpacer_44 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Reset_pass_warning_layout.addItem(self.horizontalSpacer_44)

        self.Back_to_sign_in_from_pass_reset_btn = QPushButton(self.Reset_password_screen)
        self.Back_to_sign_in_from_pass_reset_btn.setObjectName(u"Back_to_sign_in_from_pass_reset_btn")
        self.Back_to_sign_in_from_pass_reset_btn.setGeometry(QRect(5, 20, 43, 39))
        self.Back_to_sign_in_from_pass_reset_btn.setCursor(QCursor(Qt.PointingHandCursor))
        icon2 = QIcon()
        icon2.addFile(u":/back/arrow_back.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.Back_to_sign_in_from_pass_reset_btn.setIcon(icon2)
        self.Back_to_sign_in_from_pass_reset_btn.setIconSize(QSize(30, 30))
        self.Back_to_sign_in_from_pass_reset_btn.setFlat(True)
        self.stackedWidget.addWidget(self.Reset_password_screen)
        self.Select_sign_up_type_screen = QWidget()
        self.Select_sign_up_type_screen.setObjectName(u"Select_sign_up_type_screen")
        self.Select_sign_up_type_screen.setMinimumSize(QSize(360, 800))
        self.Select_sign_up_type_screen.setMaximumSize(QSize(360, 800))
        self.Select_sign_up_type_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.horizontalLayoutWidget_10 = QWidget(self.Select_sign_up_type_screen)
        self.horizontalLayoutWidget_10.setObjectName(u"horizontalLayoutWidget_10")
        self.horizontalLayoutWidget_10.setGeometry(QRect(0, 60, 361, 52))
        self.Sign_up_label_layout_2 = QHBoxLayout(self.horizontalLayoutWidget_10)
        self.Sign_up_label_layout_2.setObjectName(u"Sign_up_label_layout_2")
        self.Sign_up_label_layout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_15 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_label_layout_2.addItem(self.horizontalSpacer_15)

        self.label_4 = QLabel(self.horizontalLayoutWidget_10)
        self.label_4.setObjectName(u"label_4")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy4)
        self.label_4.setMinimumSize(QSize(0, 50))
        self.label_4.setMaximumSize(QSize(16777215, 50))
        font8 = QFont()
        font8.setFamilies([u"Dubai"])
        font8.setPointSize(25)
        font8.setBold(True)
        self.label_4.setFont(font8)
        self.label_4.setStyleSheet(u"color: #EAE8FF; font-size: 25pt; font-weight: 600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_4.setAlignment(Qt.AlignCenter)

        self.Sign_up_label_layout_2.addWidget(self.label_4)

        self.horizontalSpacer_16 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_label_layout_2.addItem(self.horizontalSpacer_16)

        self.gridLayoutWidget_2 = QWidget(self.Select_sign_up_type_screen)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(0, 190, 361, 100))
        self.Sign_up_select_layout = QGridLayout(self.gridLayoutWidget_2)
        self.Sign_up_select_layout.setObjectName(u"Sign_up_select_layout")
        self.Sign_up_select_layout.setVerticalSpacing(40)
        self.Sign_up_select_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_18 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_select_layout.addItem(self.horizontalSpacer_18, 1, 2, 1, 1)

        self.Sign_up_select_phone_btn = QPushButton(self.gridLayoutWidget_2)
        self.Sign_up_select_phone_btn.setObjectName(u"Sign_up_select_phone_btn")
        self.Sign_up_select_phone_btn.setFont(font3)
        self.Sign_up_select_phone_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Sign_up_select_phone_btn.setStyleSheet(u"background-color: #92B181; font-size: 10pt; color: #2D3047; min-width: 6em;border-radius: 10px;")
        self.Sign_up_select_phone_btn.setFlat(False)

        self.Sign_up_select_layout.addWidget(self.Sign_up_select_phone_btn, 1, 1, 1, 1)

        self.horizontalSpacer_17 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_select_layout.addItem(self.horizontalSpacer_17, 1, 0, 1, 1)

        self.Sign_up_select_email_btn = QPushButton(self.gridLayoutWidget_2)
        self.Sign_up_select_email_btn.setObjectName(u"Sign_up_select_email_btn")
        self.Sign_up_select_email_btn.setFont(font3)
        self.Sign_up_select_email_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Sign_up_select_email_btn.setStyleSheet(u"background-color: #92B181; font-size: 10pt; color: #2D3047; min-width: 6em;border-radius: 10px;")
        self.Sign_up_select_email_btn.setFlat(False)

        self.Sign_up_select_layout.addWidget(self.Sign_up_select_email_btn, 0, 1, 1, 1)

        self.Back_to_sign_in_btn = QPushButton(self.Select_sign_up_type_screen)
        self.Back_to_sign_in_btn.setObjectName(u"Back_to_sign_in_btn")
        self.Back_to_sign_in_btn.setGeometry(QRect(5, 20, 43, 39))
        self.Back_to_sign_in_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Back_to_sign_in_btn.setIcon(icon2)
        self.Back_to_sign_in_btn.setIconSize(QSize(30, 30))
        self.Back_to_sign_in_btn.setFlat(True)
        self.stackedWidget.addWidget(self.Select_sign_up_type_screen)
        self.Sign_up_email_screen = QWidget()
        self.Sign_up_email_screen.setObjectName(u"Sign_up_email_screen")
        self.Sign_up_email_screen.setEnabled(True)
        self.Sign_up_email_screen.setMinimumSize(QSize(360, 800))
        self.Sign_up_email_screen.setMaximumSize(QSize(360, 800))
        self.Sign_up_email_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.horizontalLayoutWidget = QWidget(self.Sign_up_email_screen)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(0, 60, 361, 52))
        self.Sign_up_label_email_layout = QHBoxLayout(self.horizontalLayoutWidget)
        self.Sign_up_label_email_layout.setObjectName(u"Sign_up_label_email_layout")
        self.Sign_up_label_email_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_11 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_label_email_layout.addItem(self.horizontalSpacer_11)

        self.label_3 = QLabel(self.horizontalLayoutWidget)
        self.label_3.setObjectName(u"label_3")
        sizePolicy4.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy4)
        self.label_3.setMinimumSize(QSize(0, 50))
        self.label_3.setMaximumSize(QSize(16777215, 50))
        self.label_3.setFont(font8)
        self.label_3.setStyleSheet(u"color: #EAE8FF; font-size: 25pt; font-weight: 600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_3.setAlignment(Qt.AlignCenter)

        self.Sign_up_label_email_layout.addWidget(self.label_3)

        self.horizontalSpacer_12 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_label_email_layout.addItem(self.horizontalSpacer_12)

        self.gridLayoutWidget_7 = QWidget(self.Sign_up_email_screen)
        self.gridLayoutWidget_7.setObjectName(u"gridLayoutWidget_7")
        self.gridLayoutWidget_7.setGeometry(QRect(-1, 170, 361, 342))
        self.Sign_up_info_email_layout = QGridLayout(self.gridLayoutWidget_7)
        self.Sign_up_info_email_layout.setObjectName(u"Sign_up_info_email_layout")
        self.Sign_up_info_email_layout.setVerticalSpacing(12)
        self.Sign_up_info_email_layout.setContentsMargins(0, 0, 0, 0)
        self.label_21 = QLabel(self.gridLayoutWidget_7)
        self.label_21.setObjectName(u"label_21")
        sizePolicy2.setHeightForWidth(self.label_21.sizePolicy().hasHeightForWidth())
        self.label_21.setSizePolicy(sizePolicy2)
        self.label_21.setFont(font2)
        self.label_21.setStyleSheet(u"font-size: 10pt; color: #EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_21.setFrameShadow(QFrame.Plain)

        self.Sign_up_info_email_layout.addWidget(self.label_21, 6, 1, 1, 1)

        self.Name_email_label = QLabel(self.gridLayoutWidget_7)
        self.Name_email_label.setObjectName(u"Name_email_label")
        sizePolicy2.setHeightForWidth(self.Name_email_label.sizePolicy().hasHeightForWidth())
        self.Name_email_label.setSizePolicy(sizePolicy2)
        self.Name_email_label.setFont(font2)
        self.Name_email_label.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")

        self.Sign_up_info_email_layout.addWidget(self.Name_email_label, 2, 1, 1, 1)

        self.Password_confirm_sign_up_email_le = QLineEdit(self.gridLayoutWidget_7)
        self.Password_confirm_sign_up_email_le.setObjectName(u"Password_confirm_sign_up_email_le")
        self.Password_confirm_sign_up_email_le.setFont(font1)
        self.Password_confirm_sign_up_email_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Password_confirm_sign_up_email_le.setMaxLength(32767)

        self.Sign_up_info_email_layout.addWidget(self.Password_confirm_sign_up_email_le, 7, 1, 1, 1)

        self.Sign_up_email_le = QLineEdit(self.gridLayoutWidget_7)
        self.Sign_up_email_le.setObjectName(u"Sign_up_email_le")
        sizePolicy1.setHeightForWidth(self.Sign_up_email_le.sizePolicy().hasHeightForWidth())
        self.Sign_up_email_le.setSizePolicy(sizePolicy1)
        self.Sign_up_email_le.setMinimumSize(QSize(200, 0))
        self.Sign_up_email_le.setFont(font1)
        self.Sign_up_email_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Sign_up_email_le.setInputMask(u"")
        self.Sign_up_email_le.setMaxLength(32767)
        self.Sign_up_email_le.setCursorPosition(0)

        self.Sign_up_info_email_layout.addWidget(self.Sign_up_email_le, 1, 1, 1, 1)

        self.Password_sign_up_email_le = QLineEdit(self.gridLayoutWidget_7)
        self.Password_sign_up_email_le.setObjectName(u"Password_sign_up_email_le")
        self.Password_sign_up_email_le.setFont(font1)
        self.Password_sign_up_email_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Password_sign_up_email_le.setMaxLength(32767)

        self.Sign_up_info_email_layout.addWidget(self.Password_sign_up_email_le, 5, 1, 1, 1)

        self.horizontalSpacer_33 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_info_email_layout.addItem(self.horizontalSpacer_33, 1, 0, 1, 1)

        self.Pass_visible_sign_up_email_btn = QPushButton(self.gridLayoutWidget_7)
        self.Pass_visible_sign_up_email_btn.setObjectName(u"Pass_visible_sign_up_email_btn")
        sizePolicy3.setHeightForWidth(self.Pass_visible_sign_up_email_btn.sizePolicy().hasHeightForWidth())
        self.Pass_visible_sign_up_email_btn.setSizePolicy(sizePolicy3)
        self.Pass_visible_sign_up_email_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Pass_visible_sign_up_email_btn.setStyleSheet(u"background-color: transparent;	border: none;")
        self.Pass_visible_sign_up_email_btn.setIcon(icon1)
        self.Pass_visible_sign_up_email_btn.setIconSize(QSize(20, 20))
        self.Pass_visible_sign_up_email_btn.setFlat(True)

        self.Sign_up_info_email_layout.addWidget(self.Pass_visible_sign_up_email_btn, 5, 2, 1, 1)

        self.label_20 = QLabel(self.gridLayoutWidget_7)
        self.label_20.setObjectName(u"label_20")
        sizePolicy2.setHeightForWidth(self.label_20.sizePolicy().hasHeightForWidth())
        self.label_20.setSizePolicy(sizePolicy2)
        self.label_20.setFont(font2)
        self.label_20.setStyleSheet(u"font-size: 10pt; color: #EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_20.setFrameShadow(QFrame.Plain)

        self.Sign_up_info_email_layout.addWidget(self.label_20, 4, 1, 1, 1)

        self.horizontalSpacer_34 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_info_email_layout.addItem(self.horizontalSpacer_34, 1, 2, 1, 1)

        self.Type_of_sign_up_email_label = QLabel(self.gridLayoutWidget_7)
        self.Type_of_sign_up_email_label.setObjectName(u"Type_of_sign_up_email_label")
        sizePolicy2.setHeightForWidth(self.Type_of_sign_up_email_label.sizePolicy().hasHeightForWidth())
        self.Type_of_sign_up_email_label.setSizePolicy(sizePolicy2)
        self.Type_of_sign_up_email_label.setFont(font2)
        self.Type_of_sign_up_email_label.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")

        self.Sign_up_info_email_layout.addWidget(self.Type_of_sign_up_email_label, 0, 1, 1, 1)

        self.Pass_confirm_visible_sign_up_email_btn = QPushButton(self.gridLayoutWidget_7)
        self.Pass_confirm_visible_sign_up_email_btn.setObjectName(u"Pass_confirm_visible_sign_up_email_btn")
        sizePolicy3.setHeightForWidth(self.Pass_confirm_visible_sign_up_email_btn.sizePolicy().hasHeightForWidth())
        self.Pass_confirm_visible_sign_up_email_btn.setSizePolicy(sizePolicy3)
        self.Pass_confirm_visible_sign_up_email_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Pass_confirm_visible_sign_up_email_btn.setStyleSheet(u"background-color: transparent;	border: none;")
        self.Pass_confirm_visible_sign_up_email_btn.setIcon(icon1)
        self.Pass_confirm_visible_sign_up_email_btn.setIconSize(QSize(20, 20))
        self.Pass_confirm_visible_sign_up_email_btn.setFlat(True)

        self.Sign_up_info_email_layout.addWidget(self.Pass_confirm_visible_sign_up_email_btn, 7, 2, 1, 1)

        self.Name_email_le = QLineEdit(self.gridLayoutWidget_7)
        self.Name_email_le.setObjectName(u"Name_email_le")
        sizePolicy1.setHeightForWidth(self.Name_email_le.sizePolicy().hasHeightForWidth())
        self.Name_email_le.setSizePolicy(sizePolicy1)
        self.Name_email_le.setMinimumSize(QSize(200, 0))
        self.Name_email_le.setFont(font1)
        self.Name_email_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Name_email_le.setInputMask(u"")
        self.Name_email_le.setMaxLength(32767)
        self.Name_email_le.setCursorPosition(0)

        self.Sign_up_info_email_layout.addWidget(self.Name_email_le, 3, 1, 1, 1)

        self.horizontalLayoutWidget_2 = QWidget(self.Sign_up_email_screen)
        self.horizontalLayoutWidget_2.setObjectName(u"horizontalLayoutWidget_2")
        self.horizontalLayoutWidget_2.setGeometry(QRect(-1, 600, 361, 80))
        self.Sign_up_btn_email_layout = QHBoxLayout(self.horizontalLayoutWidget_2)
        self.Sign_up_btn_email_layout.setObjectName(u"Sign_up_btn_email_layout")
        self.Sign_up_btn_email_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_37 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_btn_email_layout.addItem(self.horizontalSpacer_37)

        self.Sign_up_email_btn = QPushButton(self.horizontalLayoutWidget_2)
        self.Sign_up_email_btn.setObjectName(u"Sign_up_email_btn")
        self.Sign_up_email_btn.setFont(font3)
        self.Sign_up_email_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Sign_up_email_btn.setStyleSheet(u"background-color: #92B181; font-size: 10pt; color: #2D3047; min-width: 6em;border-radius: 10px;")
        self.Sign_up_email_btn.setFlat(False)

        self.Sign_up_btn_email_layout.addWidget(self.Sign_up_email_btn)

        self.horizontalSpacer_38 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_btn_email_layout.addItem(self.horizontalSpacer_38)

        self.Back_to_sign_up_select_email_btn = QPushButton(self.Sign_up_email_screen)
        self.Back_to_sign_up_select_email_btn.setObjectName(u"Back_to_sign_up_select_email_btn")
        self.Back_to_sign_up_select_email_btn.setGeometry(QRect(5, 20, 43, 39))
        self.Back_to_sign_up_select_email_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Back_to_sign_up_select_email_btn.setIcon(icon2)
        self.Back_to_sign_up_select_email_btn.setIconSize(QSize(30, 30))
        self.Back_to_sign_up_select_email_btn.setFlat(True)
        self.horizontalLayoutWidget_6 = QWidget(self.Sign_up_email_screen)
        self.horizontalLayoutWidget_6.setObjectName(u"horizontalLayoutWidget_6")
        self.horizontalLayoutWidget_6.setGeometry(QRect(0, 520, 361, 27))
        self.horizontalLayout_2 = QHBoxLayout(self.horizontalLayoutWidget_6)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_3)

        self.Warning_sign_up_email_lbl = QLabel(self.horizontalLayoutWidget_6)
        self.Warning_sign_up_email_lbl.setObjectName(u"Warning_sign_up_email_lbl")
        sizePolicy2.setHeightForWidth(self.Warning_sign_up_email_lbl.sizePolicy().hasHeightForWidth())
        self.Warning_sign_up_email_lbl.setSizePolicy(sizePolicy2)
        self.Warning_sign_up_email_lbl.setFont(font6)
        self.Warning_sign_up_email_lbl.setStyleSheet(u"color: #DE8F6E")
        self.Warning_sign_up_email_lbl.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_2.addWidget(self.Warning_sign_up_email_lbl)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_4)

        self.stackedWidget.addWidget(self.Sign_up_email_screen)
        self.Sign_up_phone_screen = QWidget()
        self.Sign_up_phone_screen.setObjectName(u"Sign_up_phone_screen")
        self.Sign_up_phone_screen.setMinimumSize(QSize(360, 800))
        self.Sign_up_phone_screen.setMaximumSize(QSize(360, 800))
        self.Sign_up_phone_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.horizontalLayoutWidget_21 = QWidget(self.Sign_up_phone_screen)
        self.horizontalLayoutWidget_21.setObjectName(u"horizontalLayoutWidget_21")
        self.horizontalLayoutWidget_21.setGeometry(QRect(0, 690, 361, 80))
        self.Sign_up_btn_phone_layout = QHBoxLayout(self.horizontalLayoutWidget_21)
        self.Sign_up_btn_phone_layout.setObjectName(u"Sign_up_btn_phone_layout")
        self.Sign_up_btn_phone_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_69 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_btn_phone_layout.addItem(self.horizontalSpacer_69)

        self.Sign_up_phone_btn = QPushButton(self.horizontalLayoutWidget_21)
        self.Sign_up_phone_btn.setObjectName(u"Sign_up_phone_btn")
        self.Sign_up_phone_btn.setFont(font3)
        self.Sign_up_phone_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Sign_up_phone_btn.setStyleSheet(u"background-color: #92B181; font-size: 10pt; color: #2D3047; min-width: 6em;border-radius: 10px;")
        self.Sign_up_phone_btn.setFlat(False)

        self.Sign_up_btn_phone_layout.addWidget(self.Sign_up_phone_btn)

        self.horizontalSpacer_70 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_btn_phone_layout.addItem(self.horizontalSpacer_70)

        self.horizontalLayoutWidget_22 = QWidget(self.Sign_up_phone_screen)
        self.horizontalLayoutWidget_22.setObjectName(u"horizontalLayoutWidget_22")
        self.horizontalLayoutWidget_22.setGeometry(QRect(1, 60, 361, 52))
        self.Sign_up_label_phone_layout = QHBoxLayout(self.horizontalLayoutWidget_22)
        self.Sign_up_label_phone_layout.setObjectName(u"Sign_up_label_phone_layout")
        self.Sign_up_label_phone_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_71 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_label_phone_layout.addItem(self.horizontalSpacer_71)

        self.label_8 = QLabel(self.horizontalLayoutWidget_22)
        self.label_8.setObjectName(u"label_8")
        sizePolicy4.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy4)
        self.label_8.setMinimumSize(QSize(0, 50))
        self.label_8.setMaximumSize(QSize(16777215, 50))
        self.label_8.setFont(font8)
        self.label_8.setStyleSheet(u"color: #EAE8FF; font-size: 25pt; font-weight: 600;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_8.setAlignment(Qt.AlignCenter)

        self.Sign_up_label_phone_layout.addWidget(self.label_8)

        self.horizontalSpacer_72 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_label_phone_layout.addItem(self.horizontalSpacer_72)

        self.gridLayoutWidget_12 = QWidget(self.Sign_up_phone_screen)
        self.gridLayoutWidget_12.setObjectName(u"gridLayoutWidget_12")
        self.gridLayoutWidget_12.setGeometry(QRect(0, 170, 361, 430))
        self.Sign_up_info_phone_layout = QGridLayout(self.gridLayoutWidget_12)
        self.Sign_up_info_phone_layout.setObjectName(u"Sign_up_info_phone_layout")
        self.Sign_up_info_phone_layout.setVerticalSpacing(12)
        self.Sign_up_info_phone_layout.setContentsMargins(0, 0, 0, 0)
        self.Password_sign_up_phone_le = QLineEdit(self.gridLayoutWidget_12)
        self.Password_sign_up_phone_le.setObjectName(u"Password_sign_up_phone_le")
        self.Password_sign_up_phone_le.setFont(font1)
        self.Password_sign_up_phone_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Password_sign_up_phone_le.setMaxLength(32767)

        self.Sign_up_info_phone_layout.addWidget(self.Password_sign_up_phone_le, 7, 1, 1, 1)

        self.Sign_up_phone_email_le = QLineEdit(self.gridLayoutWidget_12)
        self.Sign_up_phone_email_le.setObjectName(u"Sign_up_phone_email_le")
        sizePolicy1.setHeightForWidth(self.Sign_up_phone_email_le.sizePolicy().hasHeightForWidth())
        self.Sign_up_phone_email_le.setSizePolicy(sizePolicy1)
        self.Sign_up_phone_email_le.setMinimumSize(QSize(200, 0))
        self.Sign_up_phone_email_le.setFont(font1)
        self.Sign_up_phone_email_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Sign_up_phone_email_le.setInputMask(u"")
        self.Sign_up_phone_email_le.setMaxLength(32767)
        self.Sign_up_phone_email_le.setCursorPosition(0)

        self.Sign_up_info_phone_layout.addWidget(self.Sign_up_phone_email_le, 3, 1, 1, 1)

        self.Type_of_sign_up_phone_label = QLabel(self.gridLayoutWidget_12)
        self.Type_of_sign_up_phone_label.setObjectName(u"Type_of_sign_up_phone_label")
        sizePolicy2.setHeightForWidth(self.Type_of_sign_up_phone_label.sizePolicy().hasHeightForWidth())
        self.Type_of_sign_up_phone_label.setSizePolicy(sizePolicy2)
        self.Type_of_sign_up_phone_label.setFont(font2)
        self.Type_of_sign_up_phone_label.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")

        self.Sign_up_info_phone_layout.addWidget(self.Type_of_sign_up_phone_label, 0, 1, 1, 1)

        self.Pass_visible_sign_up_phone_btn = QPushButton(self.gridLayoutWidget_12)
        self.Pass_visible_sign_up_phone_btn.setObjectName(u"Pass_visible_sign_up_phone_btn")
        sizePolicy3.setHeightForWidth(self.Pass_visible_sign_up_phone_btn.sizePolicy().hasHeightForWidth())
        self.Pass_visible_sign_up_phone_btn.setSizePolicy(sizePolicy3)
        self.Pass_visible_sign_up_phone_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Pass_visible_sign_up_phone_btn.setStyleSheet(u"background-color: transparent;	border: none;")
        self.Pass_visible_sign_up_phone_btn.setIcon(icon1)
        self.Pass_visible_sign_up_phone_btn.setIconSize(QSize(20, 20))
        self.Pass_visible_sign_up_phone_btn.setFlat(True)

        self.Sign_up_info_phone_layout.addWidget(self.Pass_visible_sign_up_phone_btn, 7, 2, 1, 1)

        self.label_26 = QLabel(self.gridLayoutWidget_12)
        self.label_26.setObjectName(u"label_26")
        sizePolicy2.setHeightForWidth(self.label_26.sizePolicy().hasHeightForWidth())
        self.label_26.setSizePolicy(sizePolicy2)
        self.label_26.setFont(font2)
        self.label_26.setStyleSheet(u"font-size: 10pt; color: #EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_26.setFrameShadow(QFrame.Plain)

        self.Sign_up_info_phone_layout.addWidget(self.label_26, 6, 1, 1, 1)

        self.Pass_confirm_visible_sign_up_phone_btn = QPushButton(self.gridLayoutWidget_12)
        self.Pass_confirm_visible_sign_up_phone_btn.setObjectName(u"Pass_confirm_visible_sign_up_phone_btn")
        sizePolicy3.setHeightForWidth(self.Pass_confirm_visible_sign_up_phone_btn.sizePolicy().hasHeightForWidth())
        self.Pass_confirm_visible_sign_up_phone_btn.setSizePolicy(sizePolicy3)
        self.Pass_confirm_visible_sign_up_phone_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Pass_confirm_visible_sign_up_phone_btn.setStyleSheet(u"background-color: transparent;	border: none;")
        self.Pass_confirm_visible_sign_up_phone_btn.setIcon(icon1)
        self.Pass_confirm_visible_sign_up_phone_btn.setIconSize(QSize(20, 20))
        self.Pass_confirm_visible_sign_up_phone_btn.setFlat(True)

        self.Sign_up_info_phone_layout.addWidget(self.Pass_confirm_visible_sign_up_phone_btn, 9, 2, 1, 1)

        self.horizontalSpacer_73 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_info_phone_layout.addItem(self.horizontalSpacer_73, 1, 2, 1, 1)

        self.Sign_up_phone_le = QLineEdit(self.gridLayoutWidget_12)
        self.Sign_up_phone_le.setObjectName(u"Sign_up_phone_le")
        sizePolicy1.setHeightForWidth(self.Sign_up_phone_le.sizePolicy().hasHeightForWidth())
        self.Sign_up_phone_le.setSizePolicy(sizePolicy1)
        self.Sign_up_phone_le.setMinimumSize(QSize(200, 0))
        self.Sign_up_phone_le.setFont(font1)
        self.Sign_up_phone_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Sign_up_phone_le.setInputMask(u"")
        self.Sign_up_phone_le.setMaxLength(32767)
        self.Sign_up_phone_le.setCursorPosition(0)

        self.Sign_up_info_phone_layout.addWidget(self.Sign_up_phone_le, 1, 1, 1, 1)

        self.label_27 = QLabel(self.gridLayoutWidget_12)
        self.label_27.setObjectName(u"label_27")
        sizePolicy2.setHeightForWidth(self.label_27.sizePolicy().hasHeightForWidth())
        self.label_27.setSizePolicy(sizePolicy2)
        self.label_27.setFont(font2)
        self.label_27.setStyleSheet(u"font-size: 10pt; color: #EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.label_27.setFrameShadow(QFrame.Plain)

        self.Sign_up_info_phone_layout.addWidget(self.label_27, 8, 1, 1, 1)

        self.Email_lbl = QLabel(self.gridLayoutWidget_12)
        self.Email_lbl.setObjectName(u"Email_lbl")
        sizePolicy2.setHeightForWidth(self.Email_lbl.sizePolicy().hasHeightForWidth())
        self.Email_lbl.setSizePolicy(sizePolicy2)
        self.Email_lbl.setFont(font2)
        self.Email_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")

        self.Sign_up_info_phone_layout.addWidget(self.Email_lbl, 2, 1, 1, 1)

        self.horizontalSpacer_74 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Sign_up_info_phone_layout.addItem(self.horizontalSpacer_74, 1, 0, 1, 1)

        self.Name_phone_label = QLabel(self.gridLayoutWidget_12)
        self.Name_phone_label.setObjectName(u"Name_phone_label")
        sizePolicy2.setHeightForWidth(self.Name_phone_label.sizePolicy().hasHeightForWidth())
        self.Name_phone_label.setSizePolicy(sizePolicy2)
        self.Name_phone_label.setFont(font2)
        self.Name_phone_label.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")

        self.Sign_up_info_phone_layout.addWidget(self.Name_phone_label, 4, 1, 1, 1)

        self.Password_confirm_sign_up_phone_le = QLineEdit(self.gridLayoutWidget_12)
        self.Password_confirm_sign_up_phone_le.setObjectName(u"Password_confirm_sign_up_phone_le")
        self.Password_confirm_sign_up_phone_le.setFont(font1)
        self.Password_confirm_sign_up_phone_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Password_confirm_sign_up_phone_le.setMaxLength(32767)

        self.Sign_up_info_phone_layout.addWidget(self.Password_confirm_sign_up_phone_le, 9, 1, 1, 1)

        self.Name_phone_le = QLineEdit(self.gridLayoutWidget_12)
        self.Name_phone_le.setObjectName(u"Name_phone_le")
        sizePolicy1.setHeightForWidth(self.Name_phone_le.sizePolicy().hasHeightForWidth())
        self.Name_phone_le.setSizePolicy(sizePolicy1)
        self.Name_phone_le.setMinimumSize(QSize(200, 0))
        self.Name_phone_le.setFont(font1)
        self.Name_phone_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Name_phone_le.setInputMask(u"")
        self.Name_phone_le.setMaxLength(32767)
        self.Name_phone_le.setCursorPosition(0)

        self.Sign_up_info_phone_layout.addWidget(self.Name_phone_le, 5, 1, 1, 1)

        self.horizontalLayoutWidget_23 = QWidget(self.Sign_up_phone_screen)
        self.horizontalLayoutWidget_23.setObjectName(u"horizontalLayoutWidget_23")
        self.horizontalLayoutWidget_23.setGeometry(QRect(1, 610, 361, 27))
        self.horizontalLayout_6 = QHBoxLayout(self.horizontalLayoutWidget_23)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_75 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_75)

        self.Warning_sign_up_phone_lbl = QLabel(self.horizontalLayoutWidget_23)
        self.Warning_sign_up_phone_lbl.setObjectName(u"Warning_sign_up_phone_lbl")
        sizePolicy2.setHeightForWidth(self.Warning_sign_up_phone_lbl.sizePolicy().hasHeightForWidth())
        self.Warning_sign_up_phone_lbl.setSizePolicy(sizePolicy2)
        self.Warning_sign_up_phone_lbl.setFont(font6)
        self.Warning_sign_up_phone_lbl.setStyleSheet(u"color: #DE8F6E")
        self.Warning_sign_up_phone_lbl.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_6.addWidget(self.Warning_sign_up_phone_lbl)

        self.horizontalSpacer_76 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_76)

        self.Back_to_sign_up_select_phone_btn = QPushButton(self.Sign_up_phone_screen)
        self.Back_to_sign_up_select_phone_btn.setObjectName(u"Back_to_sign_up_select_phone_btn")
        self.Back_to_sign_up_select_phone_btn.setGeometry(QRect(6, 20, 43, 39))
        self.Back_to_sign_up_select_phone_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Back_to_sign_up_select_phone_btn.setIcon(icon2)
        self.Back_to_sign_up_select_phone_btn.setIconSize(QSize(30, 30))
        self.Back_to_sign_up_select_phone_btn.setFlat(True)
        self.stackedWidget.addWidget(self.Sign_up_phone_screen)
        self.Successful_sign_in_screen = QWidget()
        self.Successful_sign_in_screen.setObjectName(u"Successful_sign_in_screen")
        self.Successful_sign_in_screen.setEnabled(True)
        self.Successful_sign_in_screen.setMinimumSize(QSize(360, 800))
        self.Successful_sign_in_screen.setMaximumSize(QSize(360, 800))
        self.Successful_sign_in_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.horizontalLayoutWidget_7 = QWidget(self.Successful_sign_in_screen)
        self.horizontalLayoutWidget_7.setObjectName(u"horizontalLayoutWidget_7")
        self.horizontalLayoutWidget_7.setGeometry(QRect(0, 60, 361, 82))
        self.Signed_in_user_email_layout = QHBoxLayout(self.horizontalLayoutWidget_7)
        self.Signed_in_user_email_layout.setObjectName(u"Signed_in_user_email_layout")
        self.Signed_in_user_email_layout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.Signed_in_user_email_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Signed_in_user_email_layout.addItem(self.horizontalSpacer_5)

        self.label = QLabel(self.horizontalLayoutWidget_7)
        self.label.setObjectName(u"label")
        self.label.setStyleSheet(u"color: #EAE8FF; font-size: 20pt; font-weight: 600;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.label.setScaledContents(False)
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setWordWrap(True)

        self.Signed_in_user_email_layout.addWidget(self.label)

        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Signed_in_user_email_layout.addItem(self.horizontalSpacer_6)

        self.gridLayoutWidget = QWidget(self.Successful_sign_in_screen)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(0, 200, 361, 31))
        self.Successful_login_label_layout = QGridLayout(self.gridLayoutWidget)
        self.Successful_login_label_layout.setObjectName(u"Successful_login_label_layout")
        self.Successful_login_label_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Successful_login_label_layout.addItem(self.horizontalSpacer_7, 0, 0, 1, 1)

        self.Successful_email = QLabel(self.gridLayoutWidget)
        self.Successful_email.setObjectName(u"Successful_email")
        sizePolicy2.setHeightForWidth(self.Successful_email.sizePolicy().hasHeightForWidth())
        self.Successful_email.setSizePolicy(sizePolicy2)
        self.Successful_email.setFont(font2)
        self.Successful_email.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);\n"
"")
        self.Successful_email.setAlignment(Qt.AlignCenter)

        self.Successful_login_label_layout.addWidget(self.Successful_email, 0, 1, 1, 1)

        self.horizontalSpacer_8 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Successful_login_label_layout.addItem(self.horizontalSpacer_8, 0, 2, 1, 1)

        self.stackedWidget.addWidget(self.Successful_sign_in_screen)
        self.Start_screen = QWidget()
        self.Start_screen.setObjectName(u"Start_screen")
        self.Start_screen.setMinimumSize(QSize(360, 800))
        self.Start_screen.setMaximumSize(QSize(360, 800))
        self.Start_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.gridLayoutWidget_9 = QWidget(self.Start_screen)
        self.gridLayoutWidget_9.setObjectName(u"gridLayoutWidget_9")
        self.gridLayoutWidget_9.setGeometry(QRect(0, 60, 361, 72))
        self.Week_layout = QGridLayout(self.gridLayoutWidget_9)
        self.Week_layout.setObjectName(u"Week_layout")
        self.Week_layout.setVerticalSpacing(10)
        self.Week_layout.setContentsMargins(0, 0, 0, 0)
        self.Sun_lbl = QLabel(self.gridLayoutWidget_9)
        self.Sun_lbl.setObjectName(u"Sun_lbl")
        font9 = QFont()
        font9.setFamilies([u"Dubai Medium"])
        font9.setPointSize(9)
        font9.setBold(True)
        self.Sun_lbl.setFont(font9)
        self.Sun_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Sun_lbl.setAlignment(Qt.AlignCenter)

        self.Week_layout.addWidget(self.Sun_lbl, 0, 7, 1, 1)

        self.Wed_lbl = QLabel(self.gridLayoutWidget_9)
        self.Wed_lbl.setObjectName(u"Wed_lbl")
        self.Wed_lbl.setFont(font9)
        self.Wed_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Wed_lbl.setAlignment(Qt.AlignCenter)

        self.Week_layout.addWidget(self.Wed_lbl, 0, 3, 1, 1)

        self.Scroll_forward_btn = QPushButton(self.gridLayoutWidget_9)
        self.Scroll_forward_btn.setObjectName(u"Scroll_forward_btn")
        self.Scroll_forward_btn.setMinimumSize(QSize(30, 0))
        self.Scroll_forward_btn.setMaximumSize(QSize(30, 16777215))
        self.Scroll_forward_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Scroll_forward_btn.setStyleSheet(u"border-radius : 15px; border : 2px solid #2D3047")
        icon3 = QIcon()
        icon3.addFile(u":/scroll_forward/scroll_forward", QSize(), QIcon.Normal, QIcon.Off)
        self.Scroll_forward_btn.setIcon(icon3)
        self.Scroll_forward_btn.setIconSize(QSize(24, 24))
        self.Scroll_forward_btn.setFlat(True)

        self.Week_layout.addWidget(self.Scroll_forward_btn, 1, 8, 1, 1)

        self.Thu_lbl = QLabel(self.gridLayoutWidget_9)
        self.Thu_lbl.setObjectName(u"Thu_lbl")
        self.Thu_lbl.setFont(font9)
        self.Thu_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Thu_lbl.setAlignment(Qt.AlignCenter)

        self.Week_layout.addWidget(self.Thu_lbl, 0, 4, 1, 1)

        self.Mon_lbl = QLabel(self.gridLayoutWidget_9)
        self.Mon_lbl.setObjectName(u"Mon_lbl")
        self.Mon_lbl.setFont(font9)
        self.Mon_lbl.setCursor(QCursor(Qt.ArrowCursor))
        self.Mon_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Mon_lbl.setAlignment(Qt.AlignCenter)

        self.Week_layout.addWidget(self.Mon_lbl, 0, 1, 1, 1)

        self.Scroll_backward_btn = QPushButton(self.gridLayoutWidget_9)
        self.Scroll_backward_btn.setObjectName(u"Scroll_backward_btn")
        self.Scroll_backward_btn.setMinimumSize(QSize(35, 0))
        self.Scroll_backward_btn.setMaximumSize(QSize(30, 16777215))
        self.Scroll_backward_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Scroll_backward_btn.setStyleSheet(u"border-radius : 15px; border : 2px solid #2D3047")
        icon4 = QIcon()
        icon4.addFile(u":/scroll_backward/scroll_backward", QSize(), QIcon.Normal, QIcon.Off)
        self.Scroll_backward_btn.setIcon(icon4)
        self.Scroll_backward_btn.setIconSize(QSize(24, 24))
        self.Scroll_backward_btn.setFlat(True)

        self.Week_layout.addWidget(self.Scroll_backward_btn, 1, 0, 1, 1)

        self.Fri_lbl = QLabel(self.gridLayoutWidget_9)
        self.Fri_lbl.setObjectName(u"Fri_lbl")
        self.Fri_lbl.setFont(font9)
        self.Fri_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Fri_lbl.setAlignment(Qt.AlignCenter)

        self.Week_layout.addWidget(self.Fri_lbl, 0, 5, 1, 1)

        self.Tue_lbl = QLabel(self.gridLayoutWidget_9)
        self.Tue_lbl.setObjectName(u"Tue_lbl")
        self.Tue_lbl.setFont(font9)
        self.Tue_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Tue_lbl.setAlignment(Qt.AlignCenter)

        self.Week_layout.addWidget(self.Tue_lbl, 0, 2, 1, 1)

        self.Sat_lbl = QLabel(self.gridLayoutWidget_9)
        self.Sat_lbl.setObjectName(u"Sat_lbl")
        self.Sat_lbl.setFont(font9)
        self.Sat_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Sat_lbl.setAlignment(Qt.AlignCenter)

        self.Week_layout.addWidget(self.Sat_lbl, 0, 6, 1, 1)

        self.Day1_btn = QPushButton(self.gridLayoutWidget_9)
        self.Day1_btn.setObjectName(u"Day1_btn")
        self.Day1_btn.setMinimumSize(QSize(35, 35))
        self.Day1_btn.setMaximumSize(QSize(30, 35))
        self.Day1_btn.setFont(font9)
        self.Day1_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Day1_btn.setStyleSheet(u"color: #EAE8FF; \n"
"border-radius : 17px; \n"
"border : 3px solid #2D3047")
        self.Day1_btn.setFlat(False)

        self.Week_layout.addWidget(self.Day1_btn, 1, 1, 1, 1)

        self.Day2_btn = QPushButton(self.gridLayoutWidget_9)
        self.Day2_btn.setObjectName(u"Day2_btn")
        self.Day2_btn.setMinimumSize(QSize(35, 35))
        self.Day2_btn.setMaximumSize(QSize(30, 35))
        self.Day2_btn.setFont(font9)
        self.Day2_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Day2_btn.setStyleSheet(u"color: #EAE8FF;\n"
"border-radius : 17px; \n"
"border : 3px solid #2D3047")
        self.Day2_btn.setFlat(True)

        self.Week_layout.addWidget(self.Day2_btn, 1, 2, 1, 1)

        self.Day3_btn = QPushButton(self.gridLayoutWidget_9)
        self.Day3_btn.setObjectName(u"Day3_btn")
        self.Day3_btn.setMinimumSize(QSize(35, 35))
        self.Day3_btn.setMaximumSize(QSize(30, 35))
        self.Day3_btn.setFont(font9)
        self.Day3_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Day3_btn.setStyleSheet(u"color: #EAE8FF;\n"
"border-radius : 17px; \n"
"border : 3px solid #2D3047")
        self.Day3_btn.setFlat(True)

        self.Week_layout.addWidget(self.Day3_btn, 1, 3, 1, 1)

        self.Day4_btn = QPushButton(self.gridLayoutWidget_9)
        self.Day4_btn.setObjectName(u"Day4_btn")
        self.Day4_btn.setMinimumSize(QSize(35, 35))
        self.Day4_btn.setMaximumSize(QSize(30, 35))
        self.Day4_btn.setFont(font9)
        self.Day4_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Day4_btn.setStyleSheet(u"color: #EAE8FF;\n"
"border-radius : 17px; \n"
"border : 3px solid #2D3047")
        self.Day4_btn.setFlat(True)

        self.Week_layout.addWidget(self.Day4_btn, 1, 4, 1, 1)

        self.Day5_btn = QPushButton(self.gridLayoutWidget_9)
        self.Day5_btn.setObjectName(u"Day5_btn")
        self.Day5_btn.setMinimumSize(QSize(35, 35))
        self.Day5_btn.setMaximumSize(QSize(30, 35))
        self.Day5_btn.setFont(font9)
        self.Day5_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Day5_btn.setStyleSheet(u"color: #EAE8FF;\n"
"border-radius : 17px; \n"
"border : 3px solid #2D3047")
        self.Day5_btn.setFlat(True)

        self.Week_layout.addWidget(self.Day5_btn, 1, 5, 1, 1)

        self.Day6_btn = QPushButton(self.gridLayoutWidget_9)
        self.Day6_btn.setObjectName(u"Day6_btn")
        self.Day6_btn.setMinimumSize(QSize(35, 35))
        self.Day6_btn.setMaximumSize(QSize(30, 35))
        self.Day6_btn.setFont(font9)
        self.Day6_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Day6_btn.setStyleSheet(u"color: #EAE8FF;\n"
"border-radius : 17px; \n"
"border : 3px solid #2D3047")
        self.Day6_btn.setFlat(True)

        self.Week_layout.addWidget(self.Day6_btn, 1, 6, 1, 1)

        self.Day7_btn = QPushButton(self.gridLayoutWidget_9)
        self.Day7_btn.setObjectName(u"Day7_btn")
        self.Day7_btn.setMinimumSize(QSize(35, 35))
        self.Day7_btn.setMaximumSize(QSize(30, 35))
        self.Day7_btn.setFont(font9)
        self.Day7_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Day7_btn.setStyleSheet(u"color: #EAE8FF;\n"
"border-radius : 17px; \n"
"border : 3px solid #2D3047")
        self.Day7_btn.setFlat(True)

        self.Week_layout.addWidget(self.Day7_btn, 1, 7, 1, 1)

        self.horizontalLayoutWidget_15 = QWidget(self.Start_screen)
        self.horizontalLayoutWidget_15.setObjectName(u"horizontalLayoutWidget_15")
        self.horizontalLayoutWidget_15.setGeometry(QRect(153, 10, 201, 42))
        self.Month_layout = QHBoxLayout(self.horizontalLayoutWidget_15)
        self.Month_layout.setObjectName(u"Month_layout")
        self.Month_layout.setContentsMargins(0, 0, 0, 0)
        self.Month_lbl = QLabel(self.horizontalLayoutWidget_15)
        self.Month_lbl.setObjectName(u"Month_lbl")
        self.Month_lbl.setMinimumSize(QSize(80, 0))
        self.Month_lbl.setMaximumSize(QSize(80, 16777215))
        font10 = QFont()
        font10.setFamilies([u"Dubai Medium"])
        font10.setPointSize(12)
        font10.setBold(True)
        self.Month_lbl.setFont(font10)
        self.Month_lbl.setStyleSheet(u"color: #EAE8FF; font-size: 12")
        self.Month_lbl.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.Month_layout.addWidget(self.Month_lbl)

        self.Year_lbl = QLabel(self.horizontalLayoutWidget_15)
        self.Year_lbl.setObjectName(u"Year_lbl")
        self.Year_lbl.setMinimumSize(QSize(45, 0))
        self.Year_lbl.setMaximumSize(QSize(45, 16777215))
        self.Year_lbl.setFont(font10)
        self.Year_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Year_lbl.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.Month_layout.addWidget(self.Year_lbl)

        self.Calendar_btn = QPushButton(self.horizontalLayoutWidget_15)
        self.Calendar_btn.setObjectName(u"Calendar_btn")
        self.Calendar_btn.setMinimumSize(QSize(40, 40))
        self.Calendar_btn.setMaximumSize(QSize(40, 40))
        self.Calendar_btn.setCursor(QCursor(Qt.PointingHandCursor))
        icon5 = QIcon()
        icon5.addFile(u":/month_view/month_view", QSize(), QIcon.Normal, QIcon.Off)
        self.Calendar_btn.setIcon(icon5)
        self.Calendar_btn.setIconSize(QSize(30, 30))
        self.Calendar_btn.setFlat(True)

        self.Month_layout.addWidget(self.Calendar_btn)

        self.gridLayoutWidget_5 = QWidget(self.Start_screen)
        self.gridLayoutWidget_5.setObjectName(u"gridLayoutWidget_5")
        self.gridLayoutWidget_5.setGeometry(QRect(0, 670, 364, 95))
        self.Progress_layout = QGridLayout(self.gridLayoutWidget_5)
        self.Progress_layout.setObjectName(u"Progress_layout")
        self.Progress_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_31 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Progress_layout.addItem(self.horizontalSpacer_31, 1, 0, 1, 1)

        self.progressBar = QProgressBar(self.gridLayoutWidget_5)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setMinimumSize(QSize(350, 0))
        self.progressBar.setMaximumSize(QSize(350, 16777215))
        self.progressBar.setFont(font9)
        self.progressBar.setCursor(QCursor(Qt.ArrowCursor))
        self.progressBar.setStyleSheet(u"QProgressBar::chunk {\n"
"    background-color: #72985D;\n"
"    width: 18px;\n"
"}\n"
"QProgressBar {\n"
"    border: 2px solid #EAE8FF;\n"
"	color: #EAE8FF;\n"
"    border-radius: 5px;\n"
"    text-align: center;\n"
"}")
        self.progressBar.setValue(24)

        self.Progress_layout.addWidget(self.progressBar, 1, 1, 1, 1)

        self.horizontalSpacer_32 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Progress_layout.addItem(self.horizontalSpacer_32, 1, 2, 1, 1)

        self.pushButton_4 = QPushButton(self.gridLayoutWidget_5)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setFont(font3)
        self.pushButton_4.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton_4.setStyleSheet(u"color: #EAE8FF")
        self.pushButton_4.setFlat(True)

        self.Progress_layout.addWidget(self.pushButton_4, 0, 1, 1, 1)

        self.horizontalLayoutWidget_16 = QWidget(self.Start_screen)
        self.horizontalLayoutWidget_16.setObjectName(u"horizontalLayoutWidget_16")
        self.horizontalLayoutWidget_16.setGeometry(QRect(0, 160, 361, 36))
        self.Hello_layout = QHBoxLayout(self.horizontalLayoutWidget_16)
        self.Hello_layout.setObjectName(u"Hello_layout")
        self.Hello_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_45 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Hello_layout.addItem(self.horizontalSpacer_45)

        self.Hello_lbl = QLabel(self.horizontalLayoutWidget_16)
        self.Hello_lbl.setObjectName(u"Hello_lbl")
        self.Hello_lbl.setMinimumSize(QSize(80, 0))
        self.Hello_lbl.setMaximumSize(QSize(16777215, 16777215))
        self.Hello_lbl.setFont(font10)
        self.Hello_lbl.setStyleSheet(u"color: #EAE8FF")
        self.Hello_lbl.setAlignment(Qt.AlignCenter)

        self.Hello_layout.addWidget(self.Hello_lbl)

        self.horizontalSpacer_46 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Hello_layout.addItem(self.horizontalSpacer_46)

        self.Menu_btn = QPushButton(self.Start_screen)
        self.Menu_btn.setObjectName(u"Menu_btn")
        self.Menu_btn.setGeometry(QRect(0, 20, 41, 28))
        icon6 = QIcon()
        icon6.addFile(u":/menu/menu", QSize(), QIcon.Normal, QIcon.Off)
        self.Menu_btn.setIcon(icon6)
        self.Menu_btn.setIconSize(QSize(30, 30))
        self.Menu_btn.setFlat(True)
        self.horizontalLayoutWidget_17 = QWidget(self.Start_screen)
        self.horizontalLayoutWidget_17.setObjectName(u"horizontalLayoutWidget_17")
        self.horizontalLayoutWidget_17.setGeometry(QRect(0, 290, 361, 41))
        self.Start_practice_layout = QHBoxLayout(self.horizontalLayoutWidget_17)
        self.Start_practice_layout.setObjectName(u"Start_practice_layout")
        self.Start_practice_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_47 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Start_practice_layout.addItem(self.horizontalSpacer_47)

        self.Start_practice_btn = QPushButton(self.horizontalLayoutWidget_17)
        self.Start_practice_btn.setObjectName(u"Start_practice_btn")
        font11 = QFont()
        font11.setFamilies([u"Dubai"])
        font11.setPointSize(14)
        font11.setBold(True)
        self.Start_practice_btn.setFont(font11)
        self.Start_practice_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Start_practice_btn.setStyleSheet(u"color: #2D3047; background-color:#92B181;min-width: 6em;border-radius: 10px;")

        self.Start_practice_layout.addWidget(self.Start_practice_btn)

        self.horizontalSpacer_48 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Start_practice_layout.addItem(self.horizontalSpacer_48)

        self.stackedWidget.addWidget(self.Start_screen)
        self.Menu_screen = QWidget()
        self.Menu_screen.setObjectName(u"Menu_screen")
        self.Menu_screen.setMinimumSize(QSize(360, 800))
        self.Menu_screen.setMaximumSize(QSize(360, 800))
        self.Menu_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)")
        self.horizontalLayoutWidget_18 = QWidget(self.Menu_screen)
        self.horizontalLayoutWidget_18.setObjectName(u"horizontalLayoutWidget_18")
        self.horizontalLayoutWidget_18.setGeometry(QRect(0, 690, 361, 31))
        self.Log_out_layout = QHBoxLayout(self.horizontalLayoutWidget_18)
        self.Log_out_layout.setObjectName(u"Log_out_layout")
        self.Log_out_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_49 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Log_out_layout.addItem(self.horizontalSpacer_49)

        self.Log_out_btn = QPushButton(self.horizontalLayoutWidget_18)
        self.Log_out_btn.setObjectName(u"Log_out_btn")
        self.Log_out_btn.setFont(font5)
        self.Log_out_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Log_out_btn.setStyleSheet(u"color: #2D3047; background-color:#DE8F6E;min-width: 6em;border-radius: 10px;")

        self.Log_out_layout.addWidget(self.Log_out_btn)

        self.horizontalSpacer_50 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Log_out_layout.addItem(self.horizontalSpacer_50)

        self.Back_to_start_btn = QPushButton(self.Menu_screen)
        self.Back_to_start_btn.setObjectName(u"Back_to_start_btn")
        self.Back_to_start_btn.setGeometry(QRect(0, 10, 43, 39))
        self.Back_to_start_btn.setCursor(QCursor(Qt.PointingHandCursor))
        icon7 = QIcon()
        icon7.addFile(u"Icons/arrow_back.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.Back_to_start_btn.setIcon(icon7)
        self.Back_to_start_btn.setIconSize(QSize(30, 30))
        self.Back_to_start_btn.setFlat(True)
        self.gridLayoutWidget_11 = QWidget(self.Menu_screen)
        self.gridLayoutWidget_11.setObjectName(u"gridLayoutWidget_11")
        self.gridLayoutWidget_11.setGeometry(QRect(0, 70, 351, 119))
        self.Login_information_layout = QGridLayout(self.gridLayoutWidget_11)
        self.Login_information_layout.setObjectName(u"Login_information_layout")
        self.Login_information_layout.setContentsMargins(0, 0, 0, 0)
        self.Edit_email_menu_btn = QPushButton(self.gridLayoutWidget_11)
        self.Edit_email_menu_btn.setObjectName(u"Edit_email_menu_btn")
        self.Edit_email_menu_btn.setMinimumSize(QSize(30, 30))
        self.Edit_email_menu_btn.setMaximumSize(QSize(30, 30))
        self.Edit_email_menu_btn.setCursor(QCursor(Qt.PointingHandCursor))
        icon8 = QIcon()
        icon8.addFile(u":/edit/edit", QSize(), QIcon.Normal, QIcon.Off)
        self.Edit_email_menu_btn.setIcon(icon8)
        self.Edit_email_menu_btn.setIconSize(QSize(20, 20))
        self.Edit_email_menu_btn.setFlat(True)

        self.Login_information_layout.addWidget(self.Edit_email_menu_btn, 1, 2, 1, 1)

        self.Edit_phone_menu_btn = QPushButton(self.gridLayoutWidget_11)
        self.Edit_phone_menu_btn.setObjectName(u"Edit_phone_menu_btn")
        self.Edit_phone_menu_btn.setMinimumSize(QSize(30, 30))
        self.Edit_phone_menu_btn.setMaximumSize(QSize(30, 30))
        self.Edit_phone_menu_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Edit_phone_menu_btn.setIcon(icon8)
        self.Edit_phone_menu_btn.setIconSize(QSize(20, 20))
        self.Edit_phone_menu_btn.setFlat(True)

        self.Login_information_layout.addWidget(self.Edit_phone_menu_btn, 2, 2, 1, 1)

        self.Name_acc_le = QLineEdit(self.gridLayoutWidget_11)
        self.Name_acc_le.setObjectName(u"Name_acc_le")
        self.Name_acc_le.setFont(font3)
        self.Name_acc_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Name_acc_le.setFrame(False)

        self.Login_information_layout.addWidget(self.Name_acc_le, 0, 1, 1, 1)

        self.Name_menu_lbl = QLabel(self.gridLayoutWidget_11)
        self.Name_menu_lbl.setObjectName(u"Name_menu_lbl")
        self.Name_menu_lbl.setMinimumSize(QSize(100, 0))
        self.Name_menu_lbl.setMaximumSize(QSize(100, 16777215))
        self.Name_menu_lbl.setFont(font3)
        self.Name_menu_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Name_menu_lbl.setAlignment(Qt.AlignCenter)

        self.Login_information_layout.addWidget(self.Name_menu_lbl, 0, 0, 1, 1)

        self.Email_menu_lbl = QLabel(self.gridLayoutWidget_11)
        self.Email_menu_lbl.setObjectName(u"Email_menu_lbl")
        self.Email_menu_lbl.setFont(font3)
        self.Email_menu_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Email_menu_lbl.setAlignment(Qt.AlignCenter)

        self.Login_information_layout.addWidget(self.Email_menu_lbl, 1, 0, 1, 1)

        self.Phone_menu_lbl = QLabel(self.gridLayoutWidget_11)
        self.Phone_menu_lbl.setObjectName(u"Phone_menu_lbl")
        self.Phone_menu_lbl.setFont(font3)
        self.Phone_menu_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Phone_menu_lbl.setAlignment(Qt.AlignCenter)

        self.Login_information_layout.addWidget(self.Phone_menu_lbl, 2, 0, 1, 1)

        self.Edit_name_menu_btn = QPushButton(self.gridLayoutWidget_11)
        self.Edit_name_menu_btn.setObjectName(u"Edit_name_menu_btn")
        self.Edit_name_menu_btn.setMinimumSize(QSize(30, 30))
        self.Edit_name_menu_btn.setMaximumSize(QSize(30, 30))
        self.Edit_name_menu_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Edit_name_menu_btn.setIcon(icon8)
        self.Edit_name_menu_btn.setIconSize(QSize(20, 20))
        self.Edit_name_menu_btn.setFlat(True)

        self.Login_information_layout.addWidget(self.Edit_name_menu_btn, 0, 2, 1, 1)

        self.Email_acc_le = QLineEdit(self.gridLayoutWidget_11)
        self.Email_acc_le.setObjectName(u"Email_acc_le")
        self.Email_acc_le.setFont(font3)
        self.Email_acc_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Email_acc_le.setFrame(False)

        self.Login_information_layout.addWidget(self.Email_acc_le, 1, 1, 1, 1)

        self.Phone_acc_le = QLineEdit(self.gridLayoutWidget_11)
        self.Phone_acc_le.setObjectName(u"Phone_acc_le")
        self.Phone_acc_le.setFont(font3)
        self.Phone_acc_le.setStyleSheet(u"font-size: 10pt; color: #EAE8FF; background-color: rgba(255, 255, 255, 0);")
        self.Phone_acc_le.setFrame(False)

        self.Login_information_layout.addWidget(self.Phone_acc_le, 2, 1, 1, 1)

        self.gridLayoutWidget_13 = QWidget(self.Menu_screen)
        self.gridLayoutWidget_13.setObjectName(u"gridLayoutWidget_13")
        self.gridLayoutWidget_13.setGeometry(QRect(20, 300, 321, 173))
        self.Personal_information_layout = QGridLayout(self.gridLayoutWidget_13)
        self.Personal_information_layout.setObjectName(u"Personal_information_layout")
        self.Personal_information_layout.setContentsMargins(0, 0, 0, 0)
        self.Gender_lbl = QLabel(self.gridLayoutWidget_13)
        self.Gender_lbl.setObjectName(u"Gender_lbl")
        self.Gender_lbl.setFont(font3)
        self.Gender_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Gender_lbl.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.Personal_information_layout.addWidget(self.Gender_lbl, 0, 0, 1, 1)

        self.Height_edit = QSpinBox(self.gridLayoutWidget_13)
        self.Height_edit.setObjectName(u"Height_edit")
        self.Height_edit.setFont(font9)
        self.Height_edit.setStyleSheet(u"color:#EAE8FF")
        self.Height_edit.setMaximum(250)

        self.Personal_information_layout.addWidget(self.Height_edit, 2, 1, 1, 1)

        self.Height_lbl = QLabel(self.gridLayoutWidget_13)
        self.Height_lbl.setObjectName(u"Height_lbl")
        self.Height_lbl.setFont(font3)
        self.Height_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Height_lbl.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.Personal_information_layout.addWidget(self.Height_lbl, 2, 0, 1, 1)

        self.Weight_lbl = QLabel(self.gridLayoutWidget_13)
        self.Weight_lbl.setObjectName(u"Weight_lbl")
        self.Weight_lbl.setFont(font3)
        self.Weight_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Weight_lbl.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.Personal_information_layout.addWidget(self.Weight_lbl, 3, 0, 1, 1)

        self.Weight_edit = QSpinBox(self.gridLayoutWidget_13)
        self.Weight_edit.setObjectName(u"Weight_edit")
        self.Weight_edit.setFont(font9)
        self.Weight_edit.setStyleSheet(u"color:#EAE8FF")
        self.Weight_edit.setMaximum(150)

        self.Personal_information_layout.addWidget(self.Weight_edit, 3, 1, 1, 1)

        self.Date_of_birth_lbl = QLabel(self.gridLayoutWidget_13)
        self.Date_of_birth_lbl.setObjectName(u"Date_of_birth_lbl")
        self.Date_of_birth_lbl.setMinimumSize(QSize(120, 0))
        self.Date_of_birth_lbl.setMaximumSize(QSize(120, 16777215))
        self.Date_of_birth_lbl.setFont(font3)
        self.Date_of_birth_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Date_of_birth_lbl.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.Personal_information_layout.addWidget(self.Date_of_birth_lbl, 1, 0, 1, 1)

        self.Date_of_birth_edit = QDateEdit(self.gridLayoutWidget_13)
        self.Date_of_birth_edit.setObjectName(u"Date_of_birth_edit")
        self.Date_of_birth_edit.setFont(font9)
        self.Date_of_birth_edit.setStyleSheet(u"color:#EAE8FF")

        self.Personal_information_layout.addWidget(self.Date_of_birth_edit, 1, 1, 1, 1)

        self.Activity_lbl = QLabel(self.gridLayoutWidget_13)
        self.Activity_lbl.setObjectName(u"Activity_lbl")
        self.Activity_lbl.setFont(font3)
        self.Activity_lbl.setStyleSheet(u"font-size: 10pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Activity_lbl.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.Personal_information_layout.addWidget(self.Activity_lbl, 4, 0, 1, 1)

        self.Activity_select = QComboBox(self.gridLayoutWidget_13)
        self.Activity_select.setObjectName(u"Activity_select")
        self.Activity_select.setFont(font9)
        self.Activity_select.setStyleSheet(u"color:#EAE8FF")
        self.Activity_select.setMaxVisibleItems(5)
        self.Activity_select.setFrame(False)

        self.Personal_information_layout.addWidget(self.Activity_select, 4, 1, 1, 1)

        self.Gender_select = QComboBox(self.gridLayoutWidget_13)
        self.Gender_select.setObjectName(u"Gender_select")
        self.Gender_select.setFont(font9)
        self.Gender_select.setStyleSheet(u"color:#EAE8FF")
        self.Gender_select.setMaxVisibleItems(5)
        self.Gender_select.setFrame(False)

        self.Personal_information_layout.addWidget(self.Gender_select, 0, 1, 1, 1)

        self.Personal_info_lbl = QLabel(self.Menu_screen)
        self.Personal_info_lbl.setObjectName(u"Personal_info_lbl")
        self.Personal_info_lbl.setGeometry(QRect(10, 260, 341, 29))
        self.Personal_info_lbl.setFont(font10)
        self.Personal_info_lbl.setStyleSheet(u"font-size: 12pt; color:#92B181;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Personal_info_lbl.setAlignment(Qt.AlignCenter)
        self.Account_info_lbl = QLabel(self.Menu_screen)
        self.Account_info_lbl.setObjectName(u"Account_info_lbl")
        self.Account_info_lbl.setGeometry(QRect(10, 40, 341, 29))
        self.Account_info_lbl.setFont(font10)
        self.Account_info_lbl.setStyleSheet(u"font-size: 12pt; color:#92B181;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Account_info_lbl.setAlignment(Qt.AlignCenter)
        self.horizontalLayoutWidget_19 = QWidget(self.Menu_screen)
        self.horizontalLayoutWidget_19.setObjectName(u"horizontalLayoutWidget_19")
        self.horizontalLayoutWidget_19.setGeometry(QRect(0, 600, 361, 31))
        self.Save_profile_layout = QHBoxLayout(self.horizontalLayoutWidget_19)
        self.Save_profile_layout.setObjectName(u"Save_profile_layout")
        self.Save_profile_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_55 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Save_profile_layout.addItem(self.horizontalSpacer_55)

        self.Save_profile_btn = QPushButton(self.horizontalLayoutWidget_19)
        self.Save_profile_btn.setObjectName(u"Save_profile_btn")
        self.Save_profile_btn.setFont(font5)
        self.Save_profile_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Save_profile_btn.setStyleSheet(u"color: #2D3047; background-color:#92B181;min-width: 6em;border-radius: 10px;")

        self.Save_profile_layout.addWidget(self.Save_profile_btn)

        self.horizontalSpacer_56 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Save_profile_layout.addItem(self.horizontalSpacer_56)

        self.verticalLayoutWidget_2 = QWidget(self.Menu_screen)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(9, 480, 341, 106))
        self.Exercise_lvl_layout = QVBoxLayout(self.verticalLayoutWidget_2)
        self.Exercise_lvl_layout.setObjectName(u"Exercise_lvl_layout")
        self.Exercise_lvl_layout.setContentsMargins(0, 0, 0, 0)
        self.Exercise_level_lbl = QLabel(self.verticalLayoutWidget_2)
        self.Exercise_level_lbl.setObjectName(u"Exercise_level_lbl")
        self.Exercise_level_lbl.setFont(font10)
        self.Exercise_level_lbl.setStyleSheet(u"font-size: 12pt; color:#92B181;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Exercise_level_lbl.setAlignment(Qt.AlignCenter)
        self.Exercise_level_lbl.setWordWrap(True)

        self.Exercise_lvl_layout.addWidget(self.Exercise_level_lbl)

        self.Exercise_level_slider = QSlider(self.verticalLayoutWidget_2)
        self.Exercise_level_slider.setObjectName(u"Exercise_level_slider")
        self.Exercise_level_slider.setMinimum(1)
        self.Exercise_level_slider.setMaximum(5)
        self.Exercise_level_slider.setSingleStep(1)
        self.Exercise_level_slider.setPageStep(1)
        self.Exercise_level_slider.setValue(3)
        self.Exercise_level_slider.setSliderPosition(3)
        self.Exercise_level_slider.setOrientation(Qt.Horizontal)
        self.Exercise_level_slider.setTickPosition(QSlider.TicksBelow)
        self.Exercise_level_slider.setTickInterval(0)

        self.Exercise_lvl_layout.addWidget(self.Exercise_level_slider)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setSpacing(70)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_2 = QLabel(self.verticalLayoutWidget_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font3)
        self.label_2.setStyleSheet(u"font-size: 10pt; color:#92B181\n"
";")
        self.label_2.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.label_2)

        self.label_5 = QLabel(self.verticalLayoutWidget_2)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font3)
        self.label_5.setStyleSheet(u"font-size: 10pt; color:#A09C6C;")
        self.label_5.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.label_5)

        self.label_6 = QLabel(self.verticalLayoutWidget_2)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setFont(font3)
        self.label_6.setStyleSheet(u"font-size: 10pt; color:#AD8757;")
        self.label_6.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.label_6)

        self.label_9 = QLabel(self.verticalLayoutWidget_2)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font3)
        self.label_9.setStyleSheet(u"font-size: 10pt; color:#BB7142;")
        self.label_9.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.label_9)

        self.label_7 = QLabel(self.verticalLayoutWidget_2)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font3)
        self.label_7.setStyleSheet(u"font-size: 10pt; color:#C85C2D;")
        self.label_7.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.label_7)


        self.Exercise_lvl_layout.addLayout(self.horizontalLayout_7)

        self.horizontalLayoutWidget_20 = QWidget(self.Menu_screen)
        self.horizontalLayoutWidget_20.setObjectName(u"horizontalLayoutWidget_20")
        self.horizontalLayoutWidget_20.setGeometry(QRect(0, 220, 361, 31))
        self.Save_profile_layout_2 = QHBoxLayout(self.horizontalLayoutWidget_20)
        self.Save_profile_layout_2.setObjectName(u"Save_profile_layout_2")
        self.Save_profile_layout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_57 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Save_profile_layout_2.addItem(self.horizontalSpacer_57)

        self.Save_acc_btn = QPushButton(self.horizontalLayoutWidget_20)
        self.Save_acc_btn.setObjectName(u"Save_acc_btn")
        self.Save_acc_btn.setFont(font5)
        self.Save_acc_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Save_acc_btn.setStyleSheet(u"color: #2D3047; background-color:#92B181;min-width: 6em;border-radius: 10px;")

        self.Save_profile_layout_2.addWidget(self.Save_acc_btn)

        self.horizontalSpacer_58 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Save_profile_layout_2.addItem(self.horizontalSpacer_58)

        self.horizontalLayoutWidget_24 = QWidget(self.Menu_screen)
        self.horizontalLayoutWidget_24.setObjectName(u"horizontalLayoutWidget_24")
        self.horizontalLayoutWidget_24.setGeometry(QRect(0, 190, 361, 27))
        self.Acc_warning_layout = QHBoxLayout(self.horizontalLayoutWidget_24)
        self.Acc_warning_layout.setObjectName(u"Acc_warning_layout")
        self.Acc_warning_layout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_77 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Acc_warning_layout.addItem(self.horizontalSpacer_77)

        self.Acc_warning_lbl = QLabel(self.horizontalLayoutWidget_24)
        self.Acc_warning_lbl.setObjectName(u"Acc_warning_lbl")
        sizePolicy2.setHeightForWidth(self.Acc_warning_lbl.sizePolicy().hasHeightForWidth())
        self.Acc_warning_lbl.setSizePolicy(sizePolicy2)
        self.Acc_warning_lbl.setFont(font6)
        self.Acc_warning_lbl.setStyleSheet(u"color: #DE8F6E")
        self.Acc_warning_lbl.setAlignment(Qt.AlignCenter)

        self.Acc_warning_layout.addWidget(self.Acc_warning_lbl)

        self.horizontalSpacer_78 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Acc_warning_layout.addItem(self.horizontalSpacer_78)

        self.stackedWidget.addWidget(self.Menu_screen)
        self.Calendar_screen = QWidget()
        self.Calendar_screen.setObjectName(u"Calendar_screen")
        self.Calendar_screen.setMinimumSize(QSize(360, 800))
        self.Calendar_screen.setMaximumSize(QSize(360, 800))
        self.Calendar_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)\n"
"")
        self.verticalLayoutWidget = QWidget(self.Calendar_screen)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(0, 60, 371, 371))
        self.Calendar_layout = QVBoxLayout(self.verticalLayoutWidget)
        self.Calendar_layout.setObjectName(u"Calendar_layout")
        self.Calendar_layout.setContentsMargins(0, 0, 0, 0)
        self.Calendar = QCalendarWidget(self.verticalLayoutWidget)
        self.Calendar.setObjectName(u"Calendar")
        self.Calendar.setMinimumSize(QSize(360, 360))
        self.Calendar.setMaximumSize(QSize(360, 360))
        font12 = QFont()
        font12.setFamilies([u"Dubai Medium"])
        font12.setPointSize(11)
        font12.setBold(True)
        self.Calendar.setFont(font12)
        self.Calendar.setStyleSheet(u"QCalendarWidget QWidget { alternate-background-color: #5F7E4E; color: #EAE8FF }\n"
"QCalendarWidget QToolButton#qt_calendar_prevmonth {qproperty-icon:url(:/scroll_backward/scroll_backward); qproperty-iconSize: 28px;}\n"
"QCalendarWidget QToolButton#qt_calendar_nextmonth {qproperty-icon: url(:/scroll_forward/scroll_forward); qproperty-iconSize: 28px;}\n"
"QCalendarWidget QToolButton::menu-indicator {image: none;}\n"
"")
        self.Calendar.setGridVisible(False)
        self.Calendar.setVerticalHeaderFormat(QCalendarWidget.NoVerticalHeader)
        self.Calendar.setNavigationBarVisible(True)
        self.Calendar.setDateEditEnabled(True)
        self.Calendar.setDateEditAcceptDelay(1500)

        self.Calendar_layout.addWidget(self.Calendar)

        self.Back_to_start_from_calendar_btn = QPushButton(self.Calendar_screen)
        self.Back_to_start_from_calendar_btn.setObjectName(u"Back_to_start_from_calendar_btn")
        self.Back_to_start_from_calendar_btn.setGeometry(QRect(0, 10, 43, 39))
        self.Back_to_start_from_calendar_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Back_to_start_from_calendar_btn.setIcon(icon7)
        self.Back_to_start_from_calendar_btn.setIconSize(QSize(30, 30))
        self.Back_to_start_from_calendar_btn.setFlat(True)
        self.stackedWidget.addWidget(self.Calendar_screen)
        self.Practice_screen = QWidget()
        self.Practice_screen.setObjectName(u"Practice_screen")
        self.Practice_screen.setMinimumSize(QSize(360, 800))
        self.Practice_screen.setMaximumSize(QSize(360, 800))
        self.Practice_screen.setStyleSheet(u"background-color: rgb(45, 48, 71)\n"
"")
        self.gridLayoutWidget_4 = QWidget(self.Practice_screen)
        self.gridLayoutWidget_4.setObjectName(u"gridLayoutWidget_4")
        self.gridLayoutWidget_4.setGeometry(QRect(20, 150, 324, 312))
        self.gridLayout_2 = QGridLayout(self.gridLayoutWidget_4)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.Practice_image = QLabel(self.gridLayoutWidget_4)
        self.Practice_image.setObjectName(u"Practice_image")
        self.Practice_image.setMinimumSize(QSize(310, 310))
        self.Practice_image.setMaximumSize(QSize(310, 310))
        self.Practice_image.setPixmap(QPixmap(u"Exercises/Vrashenie_golovoj_razminka.gif"))
        self.Practice_image.setAlignment(Qt.AlignCenter)

        self.gridLayout_2.addWidget(self.Practice_image, 0, 1, 1, 1)

        self.horizontalSpacer_53 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer_53, 0, 0, 1, 1)

        self.horizontalSpacer_54 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer_54, 0, 2, 1, 1)

        self.Back_to_start_btn_2 = QPushButton(self.Practice_screen)
        self.Back_to_start_btn_2.setObjectName(u"Back_to_start_btn_2")
        self.Back_to_start_btn_2.setGeometry(QRect(0, 10, 43, 39))
        self.Back_to_start_btn_2.setCursor(QCursor(Qt.PointingHandCursor))
        self.Back_to_start_btn_2.setIcon(icon2)
        self.Back_to_start_btn_2.setIconSize(QSize(30, 30))
        self.Back_to_start_btn_2.setFlat(True)
        self.Back_to_start_btn_3 = QPushButton(self.Practice_screen)
        self.Back_to_start_btn_3.setObjectName(u"Back_to_start_btn_3")
        self.Back_to_start_btn_3.setGeometry(QRect(0, 10, 43, 39))
        self.Back_to_start_btn_3.setCursor(QCursor(Qt.PointingHandCursor))
        self.Back_to_start_btn_3.setIcon(icon7)
        self.Back_to_start_btn_3.setIconSize(QSize(30, 30))
        self.Back_to_start_btn_3.setFlat(True)
        self.horizontalLayoutWidget_25 = QWidget(self.Practice_screen)
        self.horizontalLayoutWidget_25.setObjectName(u"horizontalLayoutWidget_25")
        self.horizontalLayoutWidget_25.setGeometry(QRect(9, 680, 341, 47))
        self.horizontalLayout_13 = QHBoxLayout(self.horizontalLayoutWidget_25)
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.horizontalLayout_13.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_29 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_13.addItem(self.horizontalSpacer_29)

        self.label_14 = QLabel(self.horizontalLayoutWidget_25)
        self.label_14.setObjectName(u"label_14")
        font13 = QFont()
        font13.setFamilies([u"Dubai Medium"])
        font13.setPointSize(16)
        font13.setBold(True)
        self.label_14.setFont(font13)
        self.label_14.setStyleSheet(u"font-size: 16pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")

        self.horizontalLayout_13.addWidget(self.label_14)

        self.Repeat_time_lbl = QLabel(self.horizontalLayoutWidget_25)
        self.Repeat_time_lbl.setObjectName(u"Repeat_time_lbl")
        self.Repeat_time_lbl.setFont(font13)
        self.Repeat_time_lbl.setStyleSheet(u"font-size: 16pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")

        self.horizontalLayout_13.addWidget(self.Repeat_time_lbl)

        self.horizontalSpacer_30 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_13.addItem(self.horizontalSpacer_30)

        self.horizontalLayoutWidget_26 = QWidget(self.Practice_screen)
        self.horizontalLayoutWidget_26.setObjectName(u"horizontalLayoutWidget_26")
        self.horizontalLayoutWidget_26.setGeometry(QRect(10, 46, 341, 91))
        self.horizontalLayout_14 = QHBoxLayout(self.horizontalLayoutWidget_26)
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.horizontalLayout_14.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_51 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_14.addItem(self.horizontalSpacer_51)

        self.Practice_title = QLabel(self.horizontalLayoutWidget_26)
        self.Practice_title.setObjectName(u"Practice_title")
        self.Practice_title.setMinimumSize(QSize(260, 0))
        self.Practice_title.setMaximumSize(QSize(260, 16777215))
        font14 = QFont()
        font14.setFamilies([u"Dubai Medium"])
        font14.setPointSize(16)
        font14.setBold(True)
        font14.setItalic(False)
        self.Practice_title.setFont(font14)
        self.Practice_title.setStyleSheet(u"font-size: 16pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Practice_title.setAlignment(Qt.AlignCenter)
        self.Practice_title.setWordWrap(True)

        self.horizontalLayout_14.addWidget(self.Practice_title)

        self.horizontalSpacer_52 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_14.addItem(self.horizontalSpacer_52)

        self.Restart_practice_btn = QPushButton(self.horizontalLayoutWidget_26)
        self.Restart_practice_btn.setObjectName(u"Restart_practice_btn")
        self.Restart_practice_btn.setMinimumSize(QSize(0, 40))
        self.Restart_practice_btn.setMaximumSize(QSize(40, 40))
        self.Restart_practice_btn.setFont(font5)
        self.Restart_practice_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Restart_practice_btn.setStyleSheet(u"")
        icon9 = QIcon()
        icon9.addFile(u"Icons/replay.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.Restart_practice_btn.setIcon(icon9)
        self.Restart_practice_btn.setIconSize(QSize(35, 35))
        self.Restart_practice_btn.setFlat(True)

        self.horizontalLayout_14.addWidget(self.Restart_practice_btn)

        self.horizontalLayoutWidget_9 = QWidget(self.Practice_screen)
        self.horizontalLayoutWidget_9.setObjectName(u"horizontalLayoutWidget_9")
        self.horizontalLayoutWidget_9.setGeometry(QRect(20, 470, 321, 201))
        self.Practice_description_layout = QHBoxLayout(self.horizontalLayoutWidget_9)
        self.Practice_description_layout.setObjectName(u"Practice_description_layout")
        self.Practice_description_layout.setContentsMargins(0, 0, 0, 0)
        self.scrollArea = QScrollArea(self.horizontalLayoutWidget_9)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setFrameShape(QFrame.NoFrame)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.scrollArea.setWidgetResizable(False)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 298, 190))
        sizePolicy5 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.scrollAreaWidgetContents.sizePolicy().hasHeightForWidth())
        self.scrollAreaWidgetContents.setSizePolicy(sizePolicy5)
        self.scrollAreaWidgetContents.setMinimumSize(QSize(0, 0))
        self.verticalLayout = QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setSizeConstraint(QLayout.SetNoConstraint)
        self.Practice_description = QLabel(self.scrollAreaWidgetContents)
        self.Practice_description.setObjectName(u"Practice_description")
        sizePolicy4.setHeightForWidth(self.Practice_description.sizePolicy().hasHeightForWidth())
        self.Practice_description.setSizePolicy(sizePolicy4)
        self.Practice_description.setMinimumSize(QSize(0, 600))
        self.Practice_description.setMaximumSize(QSize(16777215, 600))
        font15 = QFont()
        font15.setFamilies([u"Dubai Medium"])
        font15.setPointSize(12)
        self.Practice_description.setFont(font15)
        self.Practice_description.setStyleSheet(u"font-size: 12pt; color:#EAE8FF;\n"
"background-color: rgba(255, 255, 255, 0);")
        self.Practice_description.setAlignment(Qt.AlignJustify|Qt.AlignTop)
        self.Practice_description.setWordWrap(True)

        self.verticalLayout.addWidget(self.Practice_description)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.Practice_description_layout.addWidget(self.scrollArea)

        self.layoutWidget_2 = QWidget(self.Practice_screen)
        self.layoutWidget_2.setObjectName(u"layoutWidget_2")
        self.layoutWidget_2.setGeometry(QRect(10, 730, 341, 52))
        self.Practice_btns_layout_2 = QHBoxLayout(self.layoutWidget_2)
        self.Practice_btns_layout_2.setObjectName(u"Practice_btns_layout_2")
        self.Practice_btns_layout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_59 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Practice_btns_layout_2.addItem(self.horizontalSpacer_59)

        self.Previous_practice_btn = QPushButton(self.layoutWidget_2)
        self.Previous_practice_btn.setObjectName(u"Previous_practice_btn")
        self.Previous_practice_btn.setMinimumSize(QSize(0, 50))
        self.Previous_practice_btn.setMaximumSize(QSize(50, 50))
        self.Previous_practice_btn.setFont(font5)
        self.Previous_practice_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Previous_practice_btn.setLayoutDirection(Qt.LeftToRight)
        self.Previous_practice_btn.setStyleSheet(u"color: #2D3047")
        icon10 = QIcon()
        icon10.addFile(u"Icons/prev_ex.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.Previous_practice_btn.setIcon(icon10)
        self.Previous_practice_btn.setIconSize(QSize(40, 40))
        self.Previous_practice_btn.setFlat(True)

        self.Practice_btns_layout_2.addWidget(self.Previous_practice_btn)

        self.horizontalSpacer_13 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Practice_btns_layout_2.addItem(self.horizontalSpacer_13)

        self.Stop_practice_btn = QPushButton(self.layoutWidget_2)
        self.Stop_practice_btn.setObjectName(u"Stop_practice_btn")
        self.Stop_practice_btn.setMinimumSize(QSize(96, 40))
        self.Stop_practice_btn.setMaximumSize(QSize(60, 40))
        self.Stop_practice_btn.setFont(font13)
        self.Stop_practice_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Stop_practice_btn.setStyleSheet(u"color: #2D3047; background-color:#92B181;min-width: 6em;border-radius: 10px;")
        self.Stop_practice_btn.setIconSize(QSize(60, 60))
        self.Stop_practice_btn.setFlat(True)

        self.Practice_btns_layout_2.addWidget(self.Stop_practice_btn)

        self.horizontalSpacer_14 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Practice_btns_layout_2.addItem(self.horizontalSpacer_14)

        self.Skip_practice_btn = QPushButton(self.layoutWidget_2)
        self.Skip_practice_btn.setObjectName(u"Skip_practice_btn")
        self.Skip_practice_btn.setMinimumSize(QSize(0, 50))
        self.Skip_practice_btn.setMaximumSize(QSize(50, 50))
        self.Skip_practice_btn.setFont(font5)
        self.Skip_practice_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.Skip_practice_btn.setStyleSheet(u"color: #2D3047;")
        self.Skip_practice_btn.setIcon(icon10)
        self.Skip_practice_btn.setIconSize(QSize(40, 40))
        self.Skip_practice_btn.setFlat(True)

        self.Practice_btns_layout_2.addWidget(self.Skip_practice_btn)

        self.horizontalSpacer_60 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.Practice_btns_layout_2.addItem(self.horizontalSpacer_60)

        self.stackedWidget.addWidget(self.Practice_screen)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        self.stackedWidget.setCurrentIndex(0)
        self.Activity_select.setCurrentIndex(-1)
        self.Gender_select.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.Welcome_label.setText(QCoreApplication.translate("MainWindow", u"Fitness app", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"Login", None))
        self.Password_login_le.setText("")
        self.Password_login_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter yor password", None))
        self.Login_le.setText("")
        self.Login_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter your email or phone number", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"Email / Phone number", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"Password ", None))
        self.Pass_visible_login_btn.setText("")
        self.Login_btn.setText(QCoreApplication.translate("MainWindow", u"Login", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"Don't have an account?", None))
        self.Create_acc_btn.setText(QCoreApplication.translate("MainWindow", u"Sign up", None))
        self.Warning_login_lbl.setText(QCoreApplication.translate("MainWindow", u"Warning", None))
        self.Reset_pass_btn.setText(QCoreApplication.translate("MainWindow", u"Forgot your Password?", None))
        self.Change_pass_lbl.setText(QCoreApplication.translate("MainWindow", u"Reset Password", None))
        self.Login_for_reset_pass_le.setText("")
        self.Login_for_reset_pass_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter your email or phone number", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"Email / Phone number", None))
        self.Confirm_reset_pass_btn.setText(QCoreApplication.translate("MainWindow", u" Reset Password ", None))
        self.Reset_pass_warning_lbl.setText(QCoreApplication.translate("MainWindow", u"Warning", None))
        self.Back_to_sign_in_from_pass_reset_btn.setText("")
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Sign up", None))
        self.Sign_up_select_phone_btn.setText(QCoreApplication.translate("MainWindow", u" Sign up via Phone number ", None))
        self.Sign_up_select_email_btn.setText(QCoreApplication.translate("MainWindow", u"Sign up via Email", None))
        self.Back_to_sign_in_btn.setText("")
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Sign up", None))
        self.label_21.setText(QCoreApplication.translate("MainWindow", u"Confirm password", None))
        self.Name_email_label.setText(QCoreApplication.translate("MainWindow", u"Name", None))
        self.Password_confirm_sign_up_email_le.setText("")
        self.Password_confirm_sign_up_email_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter yor password", None))
        self.Sign_up_email_le.setText("")
        self.Sign_up_email_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter your email", None))
        self.Password_sign_up_email_le.setText("")
        self.Password_sign_up_email_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter yor password", None))
        self.Pass_visible_sign_up_email_btn.setText("")
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"Password ", None))
        self.Type_of_sign_up_email_label.setText(QCoreApplication.translate("MainWindow", u"Email  ", None))
        self.Pass_confirm_visible_sign_up_email_btn.setText("")
        self.Name_email_le.setText("")
        self.Name_email_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter your name", None))
        self.Sign_up_email_btn.setText(QCoreApplication.translate("MainWindow", u"Sign up", None))
        self.Back_to_sign_up_select_email_btn.setText("")
        self.Warning_sign_up_email_lbl.setText(QCoreApplication.translate("MainWindow", u"Warning", None))
        self.Sign_up_phone_btn.setText(QCoreApplication.translate("MainWindow", u"Sign up", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"Sign up", None))
        self.Password_sign_up_phone_le.setText("")
        self.Password_sign_up_phone_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter yor password", None))
        self.Sign_up_phone_email_le.setText("")
        self.Sign_up_phone_email_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter your email", None))
        self.Type_of_sign_up_phone_label.setText(QCoreApplication.translate("MainWindow", u"Phone", None))
        self.Pass_visible_sign_up_phone_btn.setText("")
        self.label_26.setText(QCoreApplication.translate("MainWindow", u"Password ", None))
        self.Pass_confirm_visible_sign_up_phone_btn.setText("")
        self.Sign_up_phone_le.setText("")
        self.Sign_up_phone_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter your phone number", None))
        self.label_27.setText(QCoreApplication.translate("MainWindow", u"Confirm password", None))
        self.Email_lbl.setText(QCoreApplication.translate("MainWindow", u"Email  ", None))
        self.Name_phone_label.setText(QCoreApplication.translate("MainWindow", u"Name", None))
        self.Password_confirm_sign_up_phone_le.setText("")
        self.Password_confirm_sign_up_phone_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter yor password", None))
        self.Name_phone_le.setText("")
        self.Name_phone_le.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Enter your name", None))
        self.Warning_sign_up_phone_lbl.setText(QCoreApplication.translate("MainWindow", u"Warning", None))
        self.Back_to_sign_up_select_phone_btn.setText("")
        self.label.setText(QCoreApplication.translate("MainWindow", u"You logged in successfuly!", None))
        self.Successful_email.setText(QCoreApplication.translate("MainWindow", u"Email  ", None))
        self.Sun_lbl.setText(QCoreApplication.translate("MainWindow", u"Sun", None))
        self.Wed_lbl.setText(QCoreApplication.translate("MainWindow", u"Wed", None))
        self.Scroll_forward_btn.setText("")
        self.Thu_lbl.setText(QCoreApplication.translate("MainWindow", u"Thu", None))
        self.Mon_lbl.setText(QCoreApplication.translate("MainWindow", u"Mon", None))
        self.Scroll_backward_btn.setText("")
        self.Fri_lbl.setText(QCoreApplication.translate("MainWindow", u"Fri", None))
        self.Tue_lbl.setText(QCoreApplication.translate("MainWindow", u"Tue", None))
        self.Sat_lbl.setText(QCoreApplication.translate("MainWindow", u"Sat", None))
        self.Day1_btn.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.Day2_btn.setText(QCoreApplication.translate("MainWindow", u"2", None))
        self.Day3_btn.setText(QCoreApplication.translate("MainWindow", u"3", None))
        self.Day4_btn.setText(QCoreApplication.translate("MainWindow", u"4", None))
        self.Day5_btn.setText(QCoreApplication.translate("MainWindow", u"5", None))
        self.Day6_btn.setText(QCoreApplication.translate("MainWindow", u"6", None))
        self.Day7_btn.setText(QCoreApplication.translate("MainWindow", u"7", None))
        self.Month_lbl.setText(QCoreApplication.translate("MainWindow", u"Month", None))
        self.Year_lbl.setText(QCoreApplication.translate("MainWindow", u"Year", None))
        self.Calendar_btn.setText("")
        self.pushButton_4.setText(QCoreApplication.translate("MainWindow", u"Your progress", None))
        self.Hello_lbl.setText(QCoreApplication.translate("MainWindow", u"Hello, Name!", None))
        self.Menu_btn.setText("")
        self.Start_practice_btn.setText(QCoreApplication.translate("MainWindow", u"  Start practice  ", None))
        self.Log_out_btn.setText(QCoreApplication.translate("MainWindow", u"Log out", None))
        self.Back_to_start_btn.setText("")
        self.Edit_email_menu_btn.setText("")
        self.Edit_phone_menu_btn.setText("")
        self.Name_menu_lbl.setText(QCoreApplication.translate("MainWindow", u"Name:", None))
        self.Email_menu_lbl.setText(QCoreApplication.translate("MainWindow", u"Email:", None))
        self.Phone_menu_lbl.setText(QCoreApplication.translate("MainWindow", u"Phone:", None))
        self.Edit_name_menu_btn.setText("")
        self.Gender_lbl.setText(QCoreApplication.translate("MainWindow", u"Gender:", None))
        self.Height_lbl.setText(QCoreApplication.translate("MainWindow", u"Height (cm):", None))
        self.Weight_lbl.setText(QCoreApplication.translate("MainWindow", u"Weight (kg):", None))
        self.Date_of_birth_lbl.setText(QCoreApplication.translate("MainWindow", u"Date of birth:", None))
        self.Activity_lbl.setText(QCoreApplication.translate("MainWindow", u"Activity level:", None))
        self.Activity_select.setCurrentText("")
        self.Gender_select.setCurrentText("")
        self.Personal_info_lbl.setText(QCoreApplication.translate("MainWindow", u"Personal information", None))
        self.Account_info_lbl.setText(QCoreApplication.translate("MainWindow", u"Account information", None))
        self.Save_profile_btn.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.Exercise_level_lbl.setText(QCoreApplication.translate("MainWindow", u"Prefered exercise level", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"2", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"3", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"4", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"5", None))
        self.Save_acc_btn.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.Acc_warning_lbl.setText(QCoreApplication.translate("MainWindow", u"Warning", None))
        self.Back_to_start_from_calendar_btn.setText("")
        self.Practice_image.setText("")
        self.Back_to_start_btn_2.setText("")
        self.Back_to_start_btn_3.setText("")
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"Repeat", None))
        self.Repeat_time_lbl.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.Practice_title.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.Restart_practice_btn.setText("")
        self.Practice_description.setText(QCoreApplication.translate("MainWindow", u"TextLabel", None))
        self.Previous_practice_btn.setText("")
        self.Stop_practice_btn.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.Skip_practice_btn.setText("")
    # retranslateUi

