from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Windows.window_controller import WindowController
from PySide6.QtGui import QMovie
from PySide6 import QtCore
from PySide6.QtCore import QTimer
import json
from PySide6.QtGui import QIcon
from Scripts.Windows import start_window
from Scripts.Windows.skip_window import SkipWindow
from Scripts.Requests.firebase_database import FirebaseDatabase
from datetime import date


class PracticeWindow(AbstractWindow):
    """Practice Window with exercises"""
    _ui = None
    _controller = None
    _movie = None
    _practice_count = 1
    _total_ex_number = None
    _ex_data = {}
    _max_timer = None
    _count_timer = None
    _profile_coefficient = None
    _skip_window = None
    _mode = None
    PROFILE_EX = "Exercise_modes"
    PROFILE_MUSCLE_GROUP = "Muscle_Group"
    DESCRIPTION_MIN_HEIGHT = 100
    SCROLL_AREA_WIDTH = 310
    SCROLL_AREA_MIN_HEIGHT = 190
    DESCRIPTION_MIN_WIDTH = 280
    DESCRIPTION_MAX_WIDTH = 300
    GIF_SPEED = 100
    GIF_SIZE = 325
    EASY_LEVEL = 0.46
    MEDIUM_LEVEL = 0.64
    HARD_LEVEL = 0.82
    DEFAULT_MODE = 1

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        #self.database.upload_exercises() upload exercises to firebase

    def show_window(self):
        self._ui.Practice_screen.show()
        self._ui.Back_to_start_btn_3.setIcon(QIcon('UI/Icons/arrow_back'))
        self._profile_coefficient = self.open_profile()
        self.ex_data = self.read_json()
        self._total_ex_number = len(self.ex_data)
        self.set_exercise(self.ex_data, self._practice_count)
        self._ui.Skip_practice_btn.clicked.connect(lambda: self.skip_ex())
        self._ui.Previous_practice_btn.clicked.connect(lambda: self.prev_ex())
        self._ui.Back_to_start_btn_3.clicked.connect(lambda: self.back_function())
        self.database = FirebaseDatabase()

    def hide_window(self):
        self._ui.Practice_screen.hide()

    def read_json(self):
        """Read Exercises json file and filter it according hardness and muscle group"""
        with open('Data/Exercises.json') as json_file:
            data = json.load(json_file)
        hardness = self.selection_of_ex_level()
        data = self.selection_of_exercise_hardness(data, hardness)
        data = self.selection_of_exercise_muscle_group(data)
        return data

    def open_profile(self):
        """Return profile coefficient (for hardness calculation) from profile"""
        with open('profile_data.json') as json_file:
            data = json.load(json_file)
        return data["profile_coefficient"]

    def set_exercise(self, data, practice):
        """Set all information for exercises"""
        self.set_image(data[practice]["Image"])
        self.set_description(data[practice]["Description"])
        self.set_title(data[practice]["Title"])
        self.set_number_of_repeat(data[practice]["Number"], data[practice]["Type"])
        self.set_buttons(data[practice]["Type"])

    def selection_of_ex_level(self):
        """Select hardness according to profile coefficient"""
        if (self._profile_coefficient >= self.EASY_LEVEL) and (self._profile_coefficient < self.MEDIUM_LEVEL):
            hardness = "Easy"
        if (self._profile_coefficient >= self.MEDIUM_LEVEL) and (self._profile_coefficient < self.HARD_LEVEL):
            hardness = "Medium"
        if (self._profile_coefficient >= self.HARD_LEVEL):
            hardness = "Hard"
        return hardness

    def selection_of_exercise_hardness(self, input_data, hardness):
        """Select exercises according to hardness"""
        return [x for x in input_data if x['WarmUp'] == True or (x['WarmUp'] == False and x['Hardness'] == hardness)]

    def selection_of_exercise_muscle_group(self, input_data):
        """Select exercises according to muscle group"""
        with open('profile_data.json') as json_file:
            data = json.load(json_file)
        if self.PROFILE_MUSCLE_GROUP not in data:
            data["Muscle_Group"] = "Core"
        muscle_group_set = ["Chest", "Legs", "Back", "Core"]
        index_of_group = muscle_group_set.index(data["Muscle_Group"])
        try:
            with open('profile_data.json') as json_file:
                data_date = json.load(json_file)
            if len(data_date["Date_of_practice"])>0:
                if data_date["Date_of_practice"][len(data_date["Date_of_practice"]) - 1] != str(date.today()):
                    index_of_group = index_of_group+1
                else:
                    index_of_group = index_of_group
        except FileNotFoundError:
            index_of_group = index_of_group+1
        if index_of_group > len(muscle_group_set)-1:
            index_of_group = 0
        muscle_group = muscle_group_set[index_of_group]
        data["Muscle_Group"] = muscle_group_set[index_of_group]
        with open('profile_data.json', 'w') as json_file:
            json.dump(data, json_file, indent=4, sort_keys=True)
        return [x for x in input_data if x['WarmUp'] is True or (x['WarmUp'] is False and x["MuscleGroup"] == muscle_group)]

    def set_image(self, image):
        """Set image for exercise"""
        movie = QMovie(image)
        movie.setSpeed(self.GIF_SPEED)
        movie.start()
        width = min(movie.currentPixmap().width(), self.GIF_SIZE)
        height = self.GIF_SIZE * min(movie.currentPixmap().height(), self.GIF_SIZE)/width
        movie.setScaledSize(QtCore.QSize(width, height))
        self._ui.Practice_image.setMovie(movie)

    def set_description(self, description):
        """Set description for exercise"""
        self._ui.Practice_description.setText(description)
        self._ui.Practice_description.setMinimumHeight(self.DESCRIPTION_MIN_HEIGHT)
        self._ui.Practice_description.adjustSize()
        self._ui.scrollAreaWidgetContents.setMinimumHeight(self._ui.Practice_description.height())
        self._ui.scrollAreaWidgetContents.adjustSize()
        if self._ui.scrollAreaWidgetContents.height() < self.SCROLL_AREA_MIN_HEIGHT:
            self._ui.Practice_description.setFixedWidth(self.DESCRIPTION_MAX_WIDTH)
            self._ui.scrollAreaWidgetContents.setFixedWidth(self.SCROLL_AREA_WIDTH)
        else:
            self._ui.Practice_description.setFixedWidth(self.DESCRIPTION_MIN_WIDTH)
            self._ui.scrollAreaWidgetContents.setFixedWidth(self.SCROLL_AREA_WIDTH)

    def set_title(self, title):
        """Set title for exercise"""
        self._ui.Practice_title.setText(title)

    def get_ex_mode(self, practice):
        """Get modification of exercise from profile"""
        with open('profile_data.json') as json_file:
            data = json.load(json_file)
        if self.PROFILE_EX not in data:
            data[self.PROFILE_EX] = {}
        if self.ex_data[practice]["ID"] not in data[self.PROFILE_EX]:
            data[self.PROFILE_EX][self.ex_data[practice]["ID"]] = self.DEFAULT_MODE
        self._mode = data[self.PROFILE_EX][self.ex_data[practice]["ID"]]
        with open('profile_data.json', 'w') as json_file:
            json.dump(data, json_file, indent=4, sort_keys=True)

    def set_number_of_repeat(self, number, ex_type):
        """Set number of repeat for exercise"""
        self.get_ex_mode(self._practice_count)
        number = int(number * self._mode)
        if ex_type == "Counter":
            self._ui.Repeat_time_lbl.setText(f"{number} times")
        elif ex_type == "Timer":
            self._ui.Repeat_time_lbl.setText(f"{number} seconds")
            self._max_timer = number

    def set_buttons(self, ex_type):
        """Set navigation buttons for exercise"""
        self._ui.Previous_practice_btn.setIcon(QIcon("UI/Icons/prev_ex.png"))
        self._ui.Skip_practice_btn.setIcon(QIcon("UI/Icons/next_ex.png"))
        if ex_type == "Counter":
            self._ui.Stop_practice_btn.setText("Finish")
            self._ui.Restart_practice_btn.hide()
            self._ui.Stop_practice_btn.clicked.connect(lambda: self.switch_ex())
        elif ex_type == "Timer":
            self._ui.Stop_practice_btn.setText("Start")
            self._ui.Restart_practice_btn.setIcon(QIcon("UI/Icons/replay.svg"))
            self.timer = QTimer()
            self._count_timer = self._max_timer
            self.timer.timeout.connect(lambda: self.change_time())
            self._ui.Stop_practice_btn.clicked.connect(lambda: self.start_timer())

    def switch_ex(self):
        """Switch to the next exercise"""
        self._ui.Stop_practice_btn.clicked.disconnect()
        self._practice_count += 1
        if self._practice_count < self._total_ex_number:
            self.set_exercise(self.ex_data, self._practice_count)
        else:
            self._practice_count = 1
            self.database.upload_db()
            date_of_practice = date.today()
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
            if len(data["Date_of_practice"]) > 0:
                if data["Date_of_practice"][len(data["Date_of_practice"]) - 1] != str(date_of_practice):
                    data["Date_of_practice"].append(str(date_of_practice))
            else:
                data["Date_of_practice"].append(str(date_of_practice))
            with open('profile_data.json', 'w') as json_file:
                json.dump(data, json_file, indent=4, sort_keys=True)
            self.database.upload_db()
            element = self._controller.contain_in_list(start_window.StartWindow.__name__)
            if element is not None:
                self._controller.select_window(element)
            else:
                self._controller.select_window(start_window.StartWindow())

    def change_time(self):
        """Change time on the screen according to timer"""
        self._count_timer -= 1
        self._ui.Repeat_time_lbl.setText(f"{self._count_timer} seconds")
        self._ui.Stop_practice_btn.clicked.disconnect()
        self._ui.Stop_practice_btn.clicked.connect(lambda: self.stop_timer())
        if self._count_timer == 0:
            self.end_timer()

    def start_timer(self):
        """Start timer for exercise"""
        self._ui.Stop_practice_btn.setText("Pause")
        self.timer.start(1000)

    def stop_timer(self):
        """Stop timer (pause)"""
        self.timer.stop()
        self._ui.Stop_practice_btn.setText("Start")
        self._ui.Stop_practice_btn.clicked.disconnect()
        self._ui.Stop_practice_btn.clicked.connect(lambda: self.start_timer())

    def end_timer(self):
        """Stop timer in the end of exercise"""
        self.timer.stop()
        self.timer.timeout.disconnect()
        self.switch_ex()

    def skip_ex(self):
        """Skip exercise"""
        if self._skip_window is None:
            self._skip_window = SkipWindow(self._ui.Practice_screen)
        self._skip_window.show_window(f"EX{self._practice_count}", self)
        #self.switch_ex()

    def prev_ex(self):
        """Go to previous exercise"""
        if self._practice_count > 1:
            self._practice_count -= 1
            self.set_exercise(self.ex_data, self._practice_count)
        else:
            self._practice_count = 1
            self.set_exercise(self.ex_data, self._practice_count)

    def back_function(self):
        """Send back to previous screen"""
        element = self._controller.contain_in_list(start_window.StartWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(start_window.StartWindow())











