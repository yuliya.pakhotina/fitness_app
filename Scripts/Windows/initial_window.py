from Scripts.Windows.window_controller import WindowController
from Scripts.Windows.welcome_window import WelcomeWindow
from PySide6.QtGui import QIcon


class InitWindow:
    """Initialization of the first Window of the App"""

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._controller.setWindowIcon(QIcon('UI/Icons/app_icon'))
        element = self._controller.contain_in_list(WelcomeWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(WelcomeWindow())
