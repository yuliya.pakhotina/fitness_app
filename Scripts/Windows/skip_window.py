from PySide6.QtWidgets import QDialog
from Scripts.skip_dialog import Ui_Dialog
import json
from Scripts.Windows.window_controller import WindowController
from Scripts.Requests.firebase_database import FirebaseDatabase



class SkipWindow(QDialog, Ui_Dialog):
    """Open skip exercise pop-up"""
    _ui = None
    _controller = None
    _practice_title = None
    _data = {}
    _practice_window = None

    EASY_MODE = 1.2
    HARD_MODE = 0.8
    PROFILE_EX = "Exercise_modes"


    def __init__(self, parent = None):
        super(SkipWindow, self).__init__(parent)
        self.setupUi(self)
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self.database = FirebaseDatabase()
        self.ui_dialog = Ui_Dialog()
        self.ui_dialog.setupUi(self)
        self.ui_dialog.Easy_btn.clicked.connect(lambda: self.easy_mode())
        self.ui_dialog.Hard_btn.clicked.connect(lambda: self.hard_mode())
        self.ui_dialog.Complete_btn.clicked.connect(lambda: self.normal_mode())

    def show_window(self, ex_title, practice_window):
        self._practice_window = practice_window
        self._practice_title = ex_title
        self.show()

    def easy_mode(self):
        """Set Easy mode"""
        self.close()
        self.update_ex_mode(self.EASY_MODE)
        self._practice_window.switch_ex()

    def hard_mode(self):
        """Set Hard mode"""
        self.close()
        self.update_ex_mode(self.HARD_MODE)
        self._practice_window.switch_ex()

    def normal_mode(self):
        """Set Normal mode"""
        self.close()
        self._practice_window.switch_ex()

    def update_ex_mode(self, mode):
        """Update exercise modification"""
        with open('profile_data.json') as json_file:
            self._data = json.load(json_file)
        self._data[self.PROFILE_EX][self._practice_title] = mode
        with open('profile_data.json', 'w') as json_file:
            json.dump(self._data, json_file, indent=4, sort_keys=True)

