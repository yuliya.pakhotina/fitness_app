from Scripts.Windows.window_controller import WindowController
import json
from Scripts.Windows.Profile.profile import UserProfile
from Scripts.Utilities.singleton_class import Singleton


class Height(Singleton):
    _ui = None
    _controller = None
    _json_string = {}
    DEFAULT_HEIGHT = 170

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._profile = UserProfile.get_instance()
        self.set_height()
        self._ui.Height_edit.valueChanged.connect(lambda: self.height_changed())

    def height_changed(self):
        self._json_string["height"] = self._ui.Height_edit.value()
        self._profile.update_profile(self._json_string)

    def set_height(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
                self._ui.Height_edit.setValue(data['height'])
                self.height_changed()
        except FileNotFoundError:
            self._ui.Height_edit.setValue(self.DEFAULT_HEIGHT)
            self._json_string["height"] = self._ui.Height_edit.value()
            self._profile.update_profile(self._json_string)