from Scripts.Windows.window_controller import WindowController
import json
import datetime
from PySide6.QtCore import QDate
from Scripts.Windows.Profile.profile import UserProfile
from Data.global_const import GlobalConst as Const
from Scripts.Utilities.singleton_class import Singleton


class DateOfBirth(Singleton):
    _ui = None
    _controller = None
    _json_string = {}
    DEFAULT_BIRTH_DATE = QDate(1990, 1, 1)

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._profile = UserProfile.get_instance()
        self.set_age()
        self._ui.Date_of_birth_edit.dateChanged.connect(lambda: self.date_changed())

    def date_changed(self):
        birth_date = self._ui.Date_of_birth_edit.date()
        today = datetime.date.today()
        age = today.year - birth_date.year() - ((today.month, today.day) < (birth_date.month(), birth_date.day()))
        if age > Const.MAX_AGE:
            age = Const.MAX_AGE-1
        if age < Const.MIN_AGE:
            age = Const.MIN_AGE
        self._json_string["age"] = age
        self._json_string["birth_date"] = self._ui.Date_of_birth_edit.date().getDate()
        self._profile.update_profile(self._json_string)

    def set_age(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
                input_date = data['birth_date']
                self._ui.Date_of_birth_edit.setDate(QDate(input_date[0], input_date[1], input_date[2]))
                self.date_changed()
        except FileNotFoundError:
            self._ui.Date_of_birth_edit.setDate(self.DEFAULT_BIRTH_DATE)
            birth_date = self._ui.Date_of_birth_edit.date()
            today = datetime.date.today()
            age = today.year - birth_date.year() - ((today.month, today.day) < (birth_date.month(), birth_date.day()))
            if age > Const.MAX_AGE:
                age = Const.MAX_AGE-1
            if age < Const.MIN_AGE:
                age = Const.MIN_AGE
            self._json_string["age"] = age
            self._json_string["birth_date"] = self._ui.Date_of_birth_edit.date().getDate()
            self._profile.update_profile(self._json_string)