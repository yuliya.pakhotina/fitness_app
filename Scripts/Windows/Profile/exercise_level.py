from Scripts.Windows.window_controller import WindowController
import json
from Scripts.Windows.Profile.profile import UserProfile
from Scripts.Utilities.singleton_class import Singleton


class ExerciseLevel(Singleton):
    _ui = None
    _controller = None
    _json_string = {}
    DEFAULT_EXERCISE = 3

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._profile = UserProfile.get_instance()
        self.set_exercise()
        self._ui.Exercise_level_slider.valueChanged.connect(lambda: self.exercise_changed())

    def exercise_changed(self):
        self._json_string["exercise_level"] = self._ui.Exercise_level_slider.value()
        self._profile.update_profile(self._json_string)

    def set_exercise(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
                self._ui.Exercise_level_slider.setValue(data['exercise_level'])
                self.exercise_changed()
        except FileNotFoundError:
            self._ui.Exercise_level_slider.setValue(self.DEFAULT_EXERCISE)
            self._json_string["exercise_level"] = self._ui.Exercise_level_slider.value()
            self._profile.update_profile(self._json_string)