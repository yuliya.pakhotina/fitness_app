from Scripts.Windows.window_controller import WindowController
from Scripts.Utilities.singleton_class import Singleton
import json
from Scripts.Requests.firebase_database import FirebaseDatabase
import pandas as pd
from decimal import Decimal


class UserProfile(Singleton):
    """Setting of user profile"""
    _ui = None
    _controller = None
    _profile = {}
    _bmi_coeff = None
    _age_coeff = None
    _activity_level_coeff = None
    _exercise_level_coeff = None
    _gender_coeff = None
    list_of_coeff = []
    _bmi_dict = {}
    _age_dict = {}
    _activity_level_dict = {}
    _gender_dict = {}
    _json_string = {}
    MIN_BMI = 0
    MAX_BMI = 100
    MIN_AGE = 18
    MAX_AGE = 100
    DEFAULT_BMI = 1
    DEFAULT_PROFILE_COEFFICIENT = 0.85

    def __init__(self):
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self.database = FirebaseDatabase()
        self.init_profile()
        self.set_profile_coefficient()

    def update_profile(self, item):
        """Update user profile"""
        self._profile.update(item)

    def save_profile(self):
        """Save profile to Firebase Database"""
        self.profile_coefficient()
        with open('profile_data.json', 'w') as outfile:
            json.dump(self._profile, outfile, indent=4, sort_keys=True)
        self.database.upload_db()

    def bmi_coefficient(self):
        """Calculate BMI coefficient"""
        bmi = self._ui.Weight_edit.value()*10000 / (self._ui.Height_edit.value() * self._ui.Height_edit.value())
        df = pd.read_excel(r'Data\profile_coefficients_data.xlsx', sheet_name='body_mass_index')
        if self._ui.Gender_select.currentText() == "Female":
            df_f = df.drop(df.filter(regex=r'^male').columns, axis=1)
            bmi_select = df_f.loc[df_f['age'] == self._profile["age"]]
        elif self._ui.Gender_select.currentText() == "Male":
            df_m = df.drop(df.filter(regex=r'^female').columns, axis=1)
            bmi_select = df_m.loc[df_m['age'] == self._profile["age"]]
        elif self._ui.Gender_select.currentText() == "Not to select":
            return self.DEFAULT_BMI
        self._bmi_dict = {(self.MIN_BMI, int(bmi_select.iloc[:, 1])): 0.75,
                          (int(bmi_select.iloc[:, 1]), int(bmi_select.iloc[:, 2])): 1,
                          (int(bmi_select.iloc[:, 2]), int(bmi_select.iloc[:, 3])): 0.75,
                          (int(bmi_select.iloc[:, 3]), self.MAX_BMI): 0.5}
        for key in self._bmi_dict:
            if Decimal(key[0]) <= Decimal(bmi) < Decimal(key[1]):
                return self._bmi_dict[key]

    def age_coefficient(self):
        """Calculate age coefficient"""
        self._age_dict = {(self.MIN_AGE, 35): 1,
                          (35, 45): 0.85,
                          (45, 55): 0.7,
                          (55, 65): 0.55,
                          (65, self.MAX_AGE): 0.4}
        for key in self._age_dict:
            if Decimal(key[0]) <= Decimal(self._profile["age"]) < Decimal(key[1]):
                return self._age_dict[key]

    def activity_level_coefficient(self):
        """Calculate activity level coefficient"""
        all_items = [self._ui.Activity_select.itemText(i) for i in range(self._ui.Activity_select.count())]
        value = 0.4
        for item in all_items:
            self._activity_level_dict[item] = value
            value += 0.3
        for key in self._activity_level_dict:
            if self._profile["activity_level"] == key:
                return self._activity_level_dict[key]

    def exercise_level_coefficient(self):
        """Calculate exercise level coefficient"""
        return self._ui.Exercise_level_slider.value()/5

    def gender_coefficient(self):
        """Calculate gender coefficient"""
        all_items = [self._ui.Gender_select.itemText(i) for i in range(self._ui.Gender_select.count())]
        value = 1
        for item in all_items:
            self._gender_dict[item] = value
            value -= 0.1
        for key in self._gender_dict:
            if self._profile["gender"] == key:
                return self._gender_dict[key]

    def profile_coefficient(self):
        """Calculate profile coefficient"""
        self._bmi_coeff = self.bmi_coefficient()
        self._age_coeff = self.age_coefficient()
        self._activity_level_coeff = self.activity_level_coefficient()
        self._exercise_level_coeff = self.exercise_level_coefficient()
        self._gender_coeff = self.gender_coefficient()
        self.list_of_coeff.append(self._bmi_coeff)
        self.list_of_coeff.append(self._age_coeff)
        self.list_of_coeff.append(self._exercise_level_coeff)
        self.list_of_coeff.append(self._exercise_level_coeff)
        self.list_of_coeff.append(self._gender_coeff)
        self._profile_coeff = sum(self.list_of_coeff)/len(self.list_of_coeff)
        self._json_string["profile_coefficient"] = self._profile_coeff
        self.update_profile(self._json_string)

    def set_profile_coefficient(self):
        """Set profile coefficient"""
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
            self._profile_coeff = data['profile_coefficient']
        except FileNotFoundError:
            self._profile_coeff = self.DEFAULT_PROFILE_COEFFICIENT
            self._json_string["profile_coefficient"] = self._profile_coeff
            self.update_profile(self._json_string)

    def init_profile(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
            self._profile = data
        except FileNotFoundError:
            pass











