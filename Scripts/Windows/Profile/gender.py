from Scripts.Windows.window_controller import WindowController
import json
from Scripts.Windows.Profile.profile import UserProfile
from Scripts.Utilities.singleton_class import Singleton


class Gender(Singleton):
    _ui = None
    _controller = None
    _json_string = {}
    DEFAULT_GENDER = "Not to select"

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._profile = UserProfile.get_instance()
        self._ui.Gender_select.addItems(["Male", "Not to select", "Female"])
        self.set_gender()
        self._ui.Gender_select.textActivated.connect(lambda: self.gender_changed())

    def gender_changed(self):
        self._json_string["gender"] = self._ui.Gender_select.currentText()
        self._profile.update_profile(self._json_string)

    def set_gender(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
                self._ui.Gender_select.setCurrentText(data['gender'])
                self.gender_changed()
        except FileNotFoundError:
            self._ui.Gender_select.setCurrentText(self.DEFAULT_GENDER)
            self._json_string["gender"] = self._ui.Gender_select.currentText()
            self._profile.update_profile(self._json_string)





