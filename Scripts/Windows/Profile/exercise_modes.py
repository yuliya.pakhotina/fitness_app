from Scripts.Windows.window_controller import WindowController
import json
from Scripts.Windows.Profile.profile import UserProfile
from Scripts.Utilities.singleton_class import Singleton


class ExerciseModes(Singleton):
    """Addition Exercise modes to profile"""
    _ui = None
    _controller = None
    _json_string = {}

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._profile = UserProfile.get_instance()
        self.set_exercise_mode()

    def set_exercise_mode(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
                if "Exercise_modes" not in data:
                    data["Exercise_modes"] = {}
                self._json_string["Exercise_modes"] = data["Exercise_modes"]
                self._profile.update_profile(self._json_string)
        except FileNotFoundError:
            self._json_string["Exercise_modes"] = {}
            self._profile.update_profile(self._json_string)
