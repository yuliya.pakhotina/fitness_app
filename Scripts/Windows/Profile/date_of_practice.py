from Scripts.Windows.window_controller import WindowController
import json
from Scripts.Windows.Profile.profile import UserProfile
from Scripts.Utilities.singleton_class import Singleton


class DateOfPractice(Singleton):
    """Addition date of practice to profile"""
    _ui = None
    _controller = None
    _json_string = {}

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._profile = UserProfile.get_instance()
        self.set_date_of_practice()

    def set_date_of_practice(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
                if "Date_of_practice" not in data:
                    data["Date_of_practice"] = []
                self._json_string["Date_of_practice"] = data["Date_of_practice"]
                self._profile.update_profile(self._json_string)
        except FileNotFoundError:
            self._json_string["Date_of_practice"] = []
            self._profile.update_profile(self._json_string)

