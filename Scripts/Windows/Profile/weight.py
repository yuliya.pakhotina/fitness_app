from Scripts.Windows.window_controller import WindowController
import json
from Scripts.Windows.Profile.profile import UserProfile
from Scripts.Utilities.singleton_class import Singleton


class Weight(Singleton):
    _ui = None
    _controller = None
    _json_string = {}
    DEFAULT_WEIGHT = 70

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._profile = UserProfile.get_instance()
        self.set_weight()
        self._ui.Weight_edit.valueChanged.connect(lambda: self.weight_changed())

    def weight_changed(self):
        self._json_string["weight"] = self._ui.Weight_edit.value()
        self._profile.update_profile(self._json_string)

    def set_weight(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
                self._ui.Weight_edit.setValue(data['weight'])
                self.weight_changed()
        except FileNotFoundError:
            self._ui.Weight_edit.setValue(self.DEFAULT_WEIGHT)
            self._json_string["weight"] = self._ui.Weight_edit.value()
            self._profile.update_profile(self._json_string)