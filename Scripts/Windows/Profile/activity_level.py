from Scripts.Windows.window_controller import WindowController
import json
from Scripts.Windows.Profile.profile import UserProfile
from Scripts.Utilities.singleton_class import Singleton


class ActivityLevel(Singleton):
    _ui = None
    _controller = None
    _json_string = {}
    DEFAULT_ACTIVITY = "Inactive life"

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._profile = UserProfile.get_instance()
        self._ui.Activity_select.addItems(["Inactive life", "Occasional physical activity", "Regular physical activity"])
        self.set_activity()
        self._ui.Activity_select.textActivated.connect(lambda: self.activity_changed())

    def activity_changed(self):
        self._json_string["activity_level"] = self._ui.Activity_select.currentText()
        self._profile.update_profile(self._json_string)

    def set_activity(self):
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
                self._ui.Activity_select.setCurrentText(data['activity_level'])
                self.activity_changed()
        except FileNotFoundError:
            self._ui.Activity_select.setCurrentText(self.DEFAULT_ACTIVITY)
            self._json_string["activity_level"] = self._ui.Activity_select.currentText()
            self._profile.update_profile(self._json_string)