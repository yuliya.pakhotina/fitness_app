from PySide6 import QtWidgets
from PySide6.QtGui import QIcon
from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Windows.window_controller import WindowController
from Scripts.Windows import select_sign_up_type_window
from Scripts.Utilities.password_visibility import PasswordVisibility
from Scripts.Utilities.create_acc import CreateAcc


class SignUpPhoneWindow(AbstractWindow, PasswordVisibility, CreateAcc):
    """Sign up to the app via phone"""
    _pass_visibility_status = False
    _confirm_pass_visibility_status = False
    _ui = None
    _controller = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Password_sign_up_phone_le.setEchoMode(QtWidgets.QLineEdit.Password)
        self._ui.Password_confirm_sign_up_phone_le.setEchoMode(QtWidgets.QLineEdit.Password)
        self._ui.Back_to_sign_up_select_phone_btn.setIcon(QIcon('UI/Icons/arrow_back'))
        self._ui.Pass_visible_sign_up_phone_btn.setIcon(QIcon('UI/Icons/visible_white'))
        self._ui.Pass_confirm_visible_sign_up_phone_btn.setIcon(QIcon('UI/Icons/visible_white'))
        self._ui.Back_to_sign_up_select_phone_btn.clicked.connect(lambda: self.back_function())
        self._ui.Pass_visible_sign_up_phone_btn.clicked.connect(
            lambda: self.password_visibility_function(self._pass_visibility_status,
                                                      self._ui.Pass_visible_sign_up_phone_btn,
                                                      self._ui.Password_sign_up_phone_le))
        self._ui.Pass_confirm_visible_sign_up_phone_btn.clicked.connect(
            lambda: self.confirm_password_visibility_function(self._confirm_pass_visibility_status,
                                                              self._ui.Pass_confirm_visible_sign_up_phone_btn,
                                                              self._ui.Password_confirm_sign_up_phone_le))
        self._ui.Sign_up_phone_btn.clicked.connect(
            lambda: self.create_acc_function(self._ui.Sign_up_phone_email_le.text(),
                                             self._ui.Name_phone_le.text(),
                                             self._ui.Password_sign_up_phone_le.text(),
                                             self._ui.Password_confirm_sign_up_phone_le.text(),
                                             self._ui.Warning_sign_up_phone_lbl,
                                             self._ui.Sign_up_phone_le.text()))

    def show_window(self):
        self._ui.Warning_sign_up_phone_lbl.setText("")
        self._ui.Sign_up_phone_screen.show()

    def hide_window(self):
        self._ui.Sign_up_phone_screen.hide()

    def back_function(self):
        """Send back to previous screen"""
        element = self._controller.contain_in_list(select_sign_up_type_window.SelectSignUpTypeWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(select_sign_up_type_window.SelectSignUpTypeWindow())

    def password_visibility_function(self, status, btn, line_edit):
        self._pass_visibility_status = self.pass_change_visibility(status, btn, line_edit)

    def confirm_password_visibility_function(self, status, btn, line_edit):
        self._confirm_pass_visibility_status = self.pass_change_visibility(status, btn, line_edit)
