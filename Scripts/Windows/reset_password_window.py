import re
from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Windows.window_controller import WindowController
from PySide6.QtGui import QIcon
import Scripts.Windows.sign_in_window as sign_in_window
import Scripts.Requests.REST_API as REST_API


class ResetPassWindow(AbstractWindow):
    """Reset password for user"""
    _ui = None
    _controller = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Reset_pass_warning_lbl.setText("")
        self._ui.Back_to_sign_in_from_pass_reset_btn.setIcon(QIcon('UI/Icons/arrow_back'))
        self._ui.Back_to_sign_in_from_pass_reset_btn.clicked.connect(lambda: self.back_function())
        self._ui.Confirm_reset_pass_btn.clicked.connect(lambda: self.reset_pass())

    def show_window(self):
        self._ui.Reset_password_screen.show()

    def hide_window(self):
        self._ui.Reset_password_screen.hide()

    def back_function(self):
        """Send back to previous screen"""
        element = self._controller.contain_in_list(sign_in_window.SignInWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(sign_in_window.SignInWindow())

    def reset_pass(self):
        """Reset password via REST API request"""
        login = self._ui.Login_for_reset_pass_le.text()
        reg_ex_email = '^[a-z0-9]+[\._-]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
        reg_ex_phone = '\+?\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4}'
        if not (re.search(reg_ex_email, login)) and not (re.search(reg_ex_phone, login)):
            self._ui.Reset_pass_warning_lbl.setText("Invalid login format.")
        else:
            rest_api = REST_API.RESTAPI()
            rest_api.reset_password(login)
            element = self._controller.contain_in_list(sign_in_window.SignInWindow.__name__)
            if element is not None:
                self._controller.select_window(element)
            else:
                self._controller.select_window(sign_in_window.SignInWindow())