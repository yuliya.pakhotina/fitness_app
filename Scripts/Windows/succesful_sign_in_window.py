import json
import os
from Scripts.Windows.abstract_window import AbstractWindow
from window_controller import WindowController
from Scripts.Windows import sign_in_window


class SuccessfulSignInWindow(AbstractWindow):
    """Successful Sign In Window"""
    _ui = None
    _controller = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self.print_email()
        self._ui.Log_out_btn.clicked.connect(lambda: self.log_out())

    def show_window(self):
        self._ui.Log_out_btn.show()
        self._ui.Confirm_log_out_lbl.hide()
        self._ui.Confirm_log_out_btn.hide()
        self._ui.Stop_log_out_btn.hide()
        self._ui.Successful_sign_in_screen.show()

    def hide_window(self):
        self._ui.Successful_sign_in_screen.hide()

    def print_email(self):
        """Print Email of signed-in User"""
        try:
            with open('account_data.json') as json_file:
                data = json.load(json_file)
                email = data['email']
            self._ui.Successful_email.setText(email)
        except FileNotFoundError:
            print("File not found")

    def log_out(self):
        """Log out of the app"""
        self._ui.Log_out_btn.hide()
        self._ui.Confirm_log_out_lbl.show()
        self._ui.Confirm_log_out_btn.show()
        self._ui.Stop_log_out_btn.show()
        self._ui.Confirm_log_out_btn.clicked.connect(lambda: self.confirm_log_out())
        self._ui.Stop_log_out_btn.clicked.connect(lambda: self.stop_log_out())

    def confirm_log_out(self):
        """Confirm log out of the app"""
        os.remove('account_data.json')
        element = self._controller.contain_in_list(sign_in_window.SignInWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(sign_in_window.SignInWindow())
        self._ui.Confirm_log_out_btn.clicked.disconnect()

    def stop_log_out(self):
        """Deny log out of the app"""
        self._ui.Log_out_btn.show()
        self._ui.Confirm_log_out_lbl.hide()
        self._ui.Confirm_log_out_btn.hide()
        self._ui.Stop_log_out_btn.hide()



