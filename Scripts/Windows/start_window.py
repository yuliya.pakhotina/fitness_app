from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Windows.window_controller import WindowController
import json
from PySide6.QtGui import QIcon, QFont
from datetime import datetime
from datetime import timedelta
from functools import partial
import calendar
from Scripts.Windows import calendar_window, menu_window, practice_window


class StartWindow(AbstractWindow):
    """Start Window for successfully signed-in user"""
    _ui = None
    _controller = None
    _count_scroll = 0
    _days = []
    _week = []
    _month = []
    _day_selection = None
    _current_day_num = 0
    _selected_day = 0
    _current_date = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._days = [self._ui.Day1_btn, self._ui.Day2_btn, self._ui.Day3_btn, self._ui.Day4_btn, self._ui.Day5_btn,
                      self._ui.Day6_btn, self._ui.Day7_btn]
        selected_day = 0
        for i in self._days:
            selected_day += 1
            i.clicked.connect(partial(self.select_day, btn=i, day=selected_day))
        self._ui.Scroll_backward_btn.setIcon(QIcon('UI/Icons/scroll_backward'))
        self._ui.Scroll_forward_btn.setIcon(QIcon('UI/Icons/scroll_forward'))
        self._ui.Calendar_btn.setIcon(QIcon('UI/Icons/month_view'))
        self._ui.Menu_btn.setIcon(QIcon('UI/Icons/menu'))
        self._ui.Scroll_backward_btn.clicked.connect(lambda: self.on_scroll_backwards())
        self._ui.Scroll_forward_btn.clicked.connect(lambda: self.on_scroll_forward())
        self._ui.Menu_btn.clicked.connect(lambda: self.on_menu())
        self._ui.Calendar_btn.clicked.connect(lambda: self.on_calendar())
        self._ui.Start_practice_btn.clicked.connect(lambda: self.start_practice())
        self._ui.progressBar.hide()
        self._ui.pushButton_4.hide()

    def show_window(self):
        self._week = []
        self.print_hello()
        self.set_day()
        self._ui.Start_screen.show()

    def hide_window(self):
        self._ui.Start_screen.hide()

    def set_day(self):
        """Set week of calendar"""
        self._current_date = datetime.now()
        current_day = self._current_date.day
        current_month = self._current_date.month
        current_year = self._current_date.year
        self._ui.Month_lbl.setText(calendar.month_name[current_month])
        self._ui.Year_lbl.setText(str(current_year))
        self._current_day_num = datetime.now().weekday()
        self._selected_day = self._current_day_num
        self._day_selection = self._current_day_num
        self._days = [self._ui.Day1_btn, self._ui.Day2_btn, self._ui.Day3_btn, self._ui.Day4_btn, self._ui.Day5_btn,
                      self._ui.Day6_btn, self._ui.Day7_btn]
        self._days[self._current_day_num].setText(str(current_day))
        for i in range(7):
            sign = 1 if i < self._current_day_num else -1
            self._week.append((datetime.now() - sign * timedelta(days=sign*(self._current_day_num-i))))
            current_dates = (datetime.now() - sign * timedelta(days=sign*(self._current_day_num-i))).day
            self._days[i].setText(str(current_dates))
            self.completed_days(self._week[i], self._days[i], i)
            if self._week[i].date() == self._current_date.date():
                self._days[i].setFont(QFont("Dubai Medium", 14, QFont.Black))
            else:
                self._days[i].setFont(QFont("Dubai Medium", 10, QFont.Bold))
        back_color = self._days[self._current_day_num].palette().button().color().name()
        if back_color == "#92b181":
            self._days[self._current_day_num].setStyleSheet(f"color: #2D3047; border-radius : 12px; border : 2px solid #DE8F6E; background-color: {back_color}")
        else:
            self._days[self._current_day_num].setStyleSheet(f"color: #EAE8FF; border-radius : 12px; border : 2px solid #DE8F6E; background-color: {back_color}")
        #self._selected_day = self._current_day_num-1

    def on_scroll_backwards(self):
        """Change calendar week after back scroll"""
        self._count_scroll += -1
        for i in range(7):
            self._week[i] = (self._week[i]-timedelta(days=7))
            self._days[i].setText(str(self._week[i].day))
            if i == self._selected_day:
                self._days[i].setStyleSheet(
                    "color: #EAE8FF; border-radius : 12px; border : 2px solid #DE8F6E; background-color: #2D3047")
            else:
                self._days[i].setStyleSheet(
                    "color: #EAE8FF; border-radius : 12px; border : 2px solid #2D3047; background-color: #2D3047")
            self.completed_days(self._week[i], self._days[i], i)
            if self._week[i].date() == self._current_date.date():
                self._days[i].setFont(QFont("Dubai Medium", 14, QFont.Bold))
            else:
                self._days[i].setFont(QFont("Dubai Medium", 10, QFont.Bold))
        self._ui.Month_lbl.setText(calendar.month_name[self._week[self._day_selection].month])
        self._ui.Year_lbl.setText(str(self._week[self._day_selection].year))

    def on_scroll_forward(self):
        """Change calendar week after forward scroll"""
        self._count_scroll += 1
        for i in range(7):
            self._week[i] = (self._week[i] + timedelta(days=7))
            self._days[i].setText(str(self._week[i].day))
            if i == self._selected_day:
                self._days[i].setStyleSheet(
                    "color: #EAE8FF; border-radius : 12px; border : 2px solid #DE8F6E; background-color: #2D3047")
            else:
                self._days[i].setStyleSheet(
                    "color: #EAE8FF; border-radius : 12px; border : 2px solid #2D3047; background-color: #2D3047")
            self.completed_days(self._week[i], self._days[i], i)
            if self._week[i].date() == self._current_date.date():
                self._days[i].setFont(QFont("Dubai Medium", 14, QFont.Bold))
            else:
                self._days[i].setFont(QFont("Dubai Medium", 10, QFont.Bold))
        self._ui.Month_lbl.setText(calendar.month_name[self._week[self._day_selection].month])
        self._ui.Year_lbl.setText(str(self._week[self._day_selection].year))

    def select_day(self, btn, day):
        """Select day of the week"""
        self._day_selection = day-1
        for i in self._days:
            back_color = i.palette().button().color().name()
            if back_color == "#92b181":
                i.setStyleSheet(f"color: #2D3047; border-radius : 12px; border : 2px solid #2D3047; background-color: {back_color}")
            else:
                i.setStyleSheet(
                    f"color: #EAE8FF; border-radius : 12px; border : 2px solid #2D3047; background-color: {back_color}")
        btn_back_color = btn.palette().button().color().name()
        if btn_back_color == "#92b181":
            btn.setStyleSheet(f"color: #2D3047; border-radius : 12px; border : 2px solid #DE8F6E; background-color: {btn_back_color}")
        else:
            btn.setStyleSheet(
                f"color: #EAE8FF; border-radius : 12px; border : 2px solid #DE8F6E; background-color: {btn_back_color}")
        self._ui.Month_lbl.setText(calendar.month_name[self._week[self._day_selection].month])
        self._selected_day = day-1

    def completed_days(self, date_of_calendar, btn_of_date, day_of_week):
        """Check if exercises were completed in week days"""
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
            if str(date_of_calendar.date()) in data["Date_of_practice"]:
                if day_of_week == self._selected_day:
                    btn_of_date.setStyleSheet("color: #2D3047; background-color: #92B181; border-radius : 12px;"
                                              " border : 2px solid #DE8F6E")
                else:
                    btn_of_date.setStyleSheet("color: #2D3047; background-color: #92B181; border-radius : 12px;"
                                                " border : 2px solid #92B181")
        except FileNotFoundError:
            pass

    def on_menu(self):
        """Go to profile Window"""
        element = self._controller.contain_in_list(menu_window.MenuWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(menu_window.MenuWindow())

    def on_calendar(self):
        """Go to calendar Window"""
        element = self._controller.contain_in_list(calendar_window.CalendarWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(calendar_window.CalendarWindow())

    def print_hello(self):
        """Print Welcome statement for user"""
        try:
            with open('account_data.json') as json_file:
                data = json.load(json_file)
                name = data['displayName']
            self._ui.Hello_lbl.setText(f"Hello, {name}!")
        except FileNotFoundError:
            print("File not found")

    def start_practice(self):
        """Start exercises"""
        element = self._controller.contain_in_list(practice_window.PracticeWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(practice_window.PracticeWindow())












