from firebase_admin import auth, exceptions
import json
import re
from Scripts.Windows.window_controller import WindowController
from Scripts.Line_edit_conditions import email_condition, name_condition, phone_condition


class EditAccInfo:
    """Edit account info"""
    _ui = None
    _controller = None
    list = []

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Save_acc_btn.clicked.connect(lambda: self.edit_info(self._ui.Name_acc_le.text(),
                                                                     self._ui.Email_acc_le.text(),
                                                                     self._ui.Phone_acc_le.text()))

    def init_list(self, email, name, phone=None):
        """Create list of conditions to be completed"""
        self.list = [email_condition.EmailCondition().is_condition_completed(email),
                     name_condition.NameCondition().is_condition_completed(name)]
        if phone:
            self.list.insert(0, phone_condition.PhoneCondition().is_condition_completed(phone))

    def edit_info(self, name, email, phone):
        """Edit info locally and on server"""
        self._ui.Acc_warning_lbl.setText("")
        self.init_list(email, name, phone)
        for item in self.list:
            if len(item) != 0:
                self._ui.Acc_warning_lbl.setText(item)
                return
        with open('account_data.json') as json_file:
            data = json.loads(json_file.read())
        try:
            if len(self._ui.Phone_acc_le.text()) == 0:
                auth.update_user(data["localId"], display_name=name, email=email)
            else:
                auth.update_user(data["localId"], display_name=name, email=email, phone_number=phone)
        except exceptions.FirebaseError as e:
            message = e.args[0]
            message = re.sub(r' \(.+\)', '', message)
            self._ui.Acc_warning_lbl.setText(message)
            return
        data['displayName'] = name
        data['email'] = email
        data['phone_number'] = phone
        json_string=json.dumps(data)
        with open('account_data.json', 'w') as outfile:
            outfile.write(json_string)
