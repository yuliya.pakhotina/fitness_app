from Scripts.Windows.window_controller import WindowController
from PySide6.QtGui import QIcon
from Scripts.Windows.Account.print_acc_info import PrintAccInfo
from Scripts.Windows.Account.edit_acc_info import EditAccInfo


class Phone(PrintAccInfo, EditAccInfo):
    _ui = None
    _controller = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Edit_phone_menu_btn.setIcon(QIcon('UI/Icons/edit'))
        self.refresh_data()
        self._ui.Phone_acc_le.setReadOnly(True)
        self._ui.Edit_phone_menu_btn.clicked.connect(lambda: self.edit_phone())

    def edit_phone(self):
        self._ui.Phone_acc_le.setReadOnly(False)
        self._ui.Phone_acc_le.setFocus()

    def refresh_data(self):
        self.print_info('phone_number', self._ui.Phone_acc_le)
        if self._ui.Phone_acc_le.text() == "None":
            self._ui.Phone_acc_le.setText("")
