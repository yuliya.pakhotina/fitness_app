from Scripts.Windows.window_controller import WindowController
from PySide6.QtGui import QIcon
from Scripts.Windows.Account.print_acc_info import PrintAccInfo
from Scripts.Windows.Account.edit_acc_info import EditAccInfo


class Email(PrintAccInfo, EditAccInfo):
    _ui = None
    _controller = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Edit_email_menu_btn.setIcon(QIcon('UI/Icons/edit'))
        self.refresh_data()
        self._ui.Email_acc_le.setReadOnly(True)
        self._ui.Edit_email_menu_btn.clicked.connect(lambda: self.edit_email())

    def edit_email(self):
        self._ui.Email_acc_le.setReadOnly(False)
        self._ui.Email_acc_le.setFocus()

    def refresh_data(self):
        self.print_info('email', self._ui.Email_acc_le)