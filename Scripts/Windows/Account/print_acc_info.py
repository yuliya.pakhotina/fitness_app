import json


class PrintAccInfo:
    """Print account info"""
    def print_info(self, info, lbl):
        """Download and print name"""
        try:
            with open('account_data.json') as json_file:
                data = json.load(json_file)
                name = data[info]
            lbl.setText(name)
        except FileNotFoundError:
            print("File not found")
