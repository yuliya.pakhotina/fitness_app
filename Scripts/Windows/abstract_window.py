from abc import ABCMeta, abstractmethod


class AbstractWindow(metaclass=ABCMeta):
    """Abstract UI Window"""

    @abstractmethod
    def show_window(self):
        '''Show window'''

    @abstractmethod
    def hide_window(self):
        '''Hide window'''

