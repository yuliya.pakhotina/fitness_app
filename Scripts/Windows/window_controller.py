from PySide6.QtWidgets import QMainWindow
from Scripts.design import Ui_MainWindow
from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Utilities.singleton_class import Singleton


class WindowController(QMainWindow, Singleton):
    """Controller for UI Windows in app"""
    list_of_windows = []
    _current_window = None

    def __init__(self):
        """Constructor"""
        super(WindowController, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.show()
        #self.thread()

    def select_window(self, window: AbstractWindow):
        """Select window to open"""
        if len(self.list_of_windows) != 0:
            for item in self.list_of_windows:
                item.hide_window()
        element = self.contain_in_list(window.__class__.__name__)
        if element is None:
            self.list_of_windows.append(window)
        window.show_window()
        self._current_window = window

    def contain_in_list(self, type_of_item):
        """Check status of window existing"""
        for obj in self.list_of_windows:
            if obj.__class__.__name__ == type_of_item:
                return obj
        return None


    '''def close_event(self, event):
        print("User closed app")
        #os.remove('account_data.json')
        event.accept()
    '''