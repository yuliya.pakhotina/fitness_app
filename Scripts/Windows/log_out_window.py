from PySide6.QtWidgets import QDialog
from Scripts.log_out_dialog import Ui_Dialog
import os
import Scripts.Windows.sign_in_window as sign_in_window
from Scripts.Windows.window_controller import WindowController


class LogOutWindow(QDialog, Ui_Dialog):
    _ui = None
    _controller = None

    def __init__(self, parent = None):
        super(LogOutWindow, self).__init__(parent)
        self.setupUi(self)
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self.ui_dialog = Ui_Dialog()
        self.ui_dialog.setupUi(self)
        self.ui_dialog.Confirm_log_out_btn.clicked.connect(lambda: self.confirm_log_out())
        self.ui_dialog.Stop_log_out_btn.clicked.connect(lambda: self.stop_log_out())

    def confirm_log_out(self):
        """Confirm log out from the app"""
        os.remove('account_data.json')
        os.remove('profile_data.json')
        element = self._controller.contain_in_list(sign_in_window.SignInWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(sign_in_window.SignInWindow())
        self.ui_dialog.Confirm_log_out_btn.clicked.disconnect()
        self.close()

    def stop_log_out(self):
        """Deny log out from the app"""
        self.close()