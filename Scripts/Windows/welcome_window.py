import os
from Scripts.Windows import sign_in_window, start_window
from Scripts.Windows.window_controller import WindowController
from Scripts.Windows.abstract_window import AbstractWindow


class WelcomeWindow(AbstractWindow):
    """Splash screen"""
    _ui = None
    _controller = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui

    def show_window(self):
        self._ui.Welcome_screen.show()
        if os.path.isfile('account_data.json'):
            element = self._controller.contain_in_list(start_window.StartWindow.__name__)
            if element is not None:
                self._controller.select_window(element)
            else:
                self._controller.select_window(start_window.StartWindow())
        else:
            element = self._controller.contain_in_list(sign_in_window.SignInWindow.__name__)
            if element is not None:
                self._controller.select_window(element)
            else:
                self._controller.select_window(sign_in_window.SignInWindow())

    def hide_window(self):
        self._ui.Welcome_screen.hide()