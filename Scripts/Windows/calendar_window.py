from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Windows.window_controller import WindowController
import Scripts.Windows.start_window as start_window
from PySide6.QtWidgets import QToolButton
from PySide6.QtGui import QIcon, QTextCharFormat, QBrush, QColor
from PySide6.QtCore import Qt, QDate
import json


class CalendarWindow(AbstractWindow):
    """Window for calendar"""

    _ui = None
    _controller = None
    _selected_date = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Back_to_start_from_calendar_btn.setIcon(QIcon('UI/Icons/arrow_back'))
        self._ui.Back_to_start_from_calendar_btn.clicked.connect(lambda: self.back_function())
        prev_btn = self._ui.Calendar.findChild(QToolButton, "qt_calendar_prevmonth")
        next_btn = self._ui.Calendar.findChild(QToolButton, "qt_calendar_nextmonth")
        prev_btn.setIcon(QIcon("UI/Icons/scroll_backward"))
        next_btn.setIcon(QIcon("UI/Icons/scroll_forward"))
        self.set_calendars()

    def show_window(self):
        self._ui.Calendar_screen.show()

    def hide_window(self):
        self._ui.Calendar_screen.hide()

    def back_function(self):
        """Send back to previous screen"""
        element = self._controller.contain_in_list(start_window.StartWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(start_window.StartWindow())

    def set_calendars(self):
        """Set format of calendar visualization"""
        fmt_weekends = QTextCharFormat()
        fmt_weekends.setForeground(QBrush(QColor('#E5A68B')))
        self._ui.Calendar.setWeekdayTextFormat(Qt.Saturday, fmt_weekends)
        self._ui.Calendar.setWeekdayTextFormat(Qt.Sunday, fmt_weekends)
        self.check_date_of_practice()

    def check_date_of_practice(self):
        """Check days in calendar if there were practice or not"""
        try:
            with open('profile_data.json') as json_file:
                data = json.load(json_file)
            date_format = "yyyy-MM-dd"
            cell_format = QTextCharFormat()
            cell_format.setBackground(QColor("green"))
            for i in data["Date_of_practice"]:
                dt = QDate.fromString(i, date_format)
                self._ui.Calendar.setDateTextFormat(dt, cell_format)
        except FileNotFoundError:
            pass




