from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Windows.window_controller import WindowController
import Scripts.Windows.sign_in_window as sign_in_window
import Scripts.Windows.sign_up_email_window as sign_up_email_window
import Scripts.Windows.sign_up_phone_window as sign_up_phone_window
from PySide6.QtGui import QIcon


class SelectSignUpTypeWindow(AbstractWindow):
    """Selection of sign up type: email or phone"""
    _ui = None
    _controller = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Back_to_sign_in_btn.setIcon(QIcon('UI/Icons/arrow_back'))
        self._ui.Back_to_sign_in_btn.clicked.connect(lambda: self.back_function())
        self._ui.Sign_up_select_email_btn.clicked.connect(lambda: self.sign_up_email())
        self._ui.Sign_up_select_phone_btn.clicked.connect(lambda: self.sign_up_phone())

    def show_window(self):
        self._ui.Select_sign_up_type_screen.show()

    def hide_window(self):
        self._ui.Select_sign_up_type_screen.hide()

    def back_function(self):
        """Send back to previous screen"""
        element = self._controller.contain_in_list(sign_in_window.SignInWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(sign_in_window.SignInWindow())

    def sign_up_email(self):
        """Open Sign up via email Window"""
        element = self._controller.contain_in_list(sign_up_email_window.SignUpEmailWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(sign_up_email_window.SignUpEmailWindow())

    def sign_up_phone(self):
        """Open Sign up via phone Window"""
        element = self._controller.contain_in_list(sign_up_phone_window.SignUpPhoneWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(sign_up_phone_window.SignUpPhoneWindow())

