from PySide6.QtGui import QIcon
from PySide6 import QtWidgets
from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Windows.window_controller import WindowController
from Scripts.Windows import reset_password_window, select_sign_up_type_window
from Scripts.Utilities.password_visibility import PasswordVisibility
from Scripts.Utilities.log_in import LogIn


class SignInWindow(AbstractWindow, PasswordVisibility, LogIn):
    """Sign in Window"""
    _visibility_status = False
    _ui = None
    _controller = None

    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Password_login_le.setEchoMode(QtWidgets.QLineEdit.Password)
        self._ui.Pass_visible_login_btn.setIcon(QIcon('UI/Icons/visible_white'))
        self._ui.Pass_visible_login_btn.clicked.connect(
            lambda: self.password_visibility_function(self._visibility_status,
                                                      self._ui.Pass_visible_login_btn,
                                                      self._ui.Password_login_le))
        self.login = LogIn()
        self._ui.Login_btn.clicked.connect(lambda: self.login.login_function(self._ui.Login_le.text(),
                                                                             self._ui.Password_login_le.text(),
                                                                             self._ui.Warning_login_lbl))
        self._ui.Create_acc_btn.clicked.connect(lambda: self.go_to_create_acc())
        self._ui.Reset_pass_btn.clicked.connect(lambda: self.reset_pass())

    def show_window(self):
        self._ui.Warning_login_lbl.setText("")
        self._ui.Sign_in_screen.show()

    def hide_window(self):
        self._ui.Sign_in_screen.hide()

    def password_visibility_function(self, status, btn, line_edit):
        self._visibility_status = self.pass_change_visibility(status, btn, line_edit)

    def go_to_create_acc(self):
        """Open Sign Up Window"""
        element = self._controller.contain_in_list(select_sign_up_type_window.SelectSignUpTypeWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(select_sign_up_type_window.SelectSignUpTypeWindow())

    def reset_pass(self):
        """Reset password"""
        element = self._controller.contain_in_list(reset_password_window.ResetPassWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(reset_password_window.ResetPassWindow())







