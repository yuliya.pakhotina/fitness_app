import Scripts.Windows.start_window as start_window
from Scripts.Windows.abstract_window import AbstractWindow
from Scripts.Windows.window_controller import WindowController
from PySide6.QtGui import QIcon
from Scripts.Windows.Account.user_name import UserName
from Scripts.Windows.Account.email import Email
from Scripts.Windows.Account.phone import Phone
from Scripts.Windows.Account.edit_acc_info import EditAccInfo
from Scripts.Windows.Profile.gender import Gender
from Scripts.Windows.Profile.date_of_birth import DateOfBirth
from Scripts.Windows.Profile.height import Height
from Scripts.Windows.Profile.weight import Weight
from Scripts.Windows.Profile.activity_level import ActivityLevel
from Scripts.Windows.Profile.exercise_level import ExerciseLevel
from Scripts.Windows.log_out_window import LogOutWindow
from Scripts.Windows.Profile.date_of_practice import DateOfPractice
from Scripts.Windows.Profile.exercise_modes import ExerciseModes
from Scripts.Windows.Profile.profile import UserProfile


class MenuWindow(AbstractWindow):
    """Profile with personal information Window"""
    _ui = None
    _controller = None
    _username = None
    _email = None
    _phone = None
    _gender = None
    _date_of_birth = None
    _height = None
    _weight = None
    _activity_level = None
    _exercise_level = None
    _date_of_practice = None
    _exercise_modes = None


    def __init__(self):
        """Constructor"""
        self._controller = WindowController.get_instance()
        self._ui = self._controller.ui
        self._ui.Log_out_btn.clicked.connect(lambda: self.log_out())
        self._ui.Back_to_start_btn.clicked.connect(lambda: self.back_function())
        self._ui.Back_to_start_btn.setIcon(QIcon('UI/Icons/arrow_back'))
        self._ui.Save_profile_btn.clicked.connect(lambda: self.save_profile())
        self._username = UserName()
        self._email = Email()
        self._phone = Phone()
        self._gender = Gender.get_instance()
        self._date_of_birth = DateOfBirth.get_instance()
        self._height = Height.get_instance()
        self._weight = Weight.get_instance()
        self._activity_level = ActivityLevel.get_instance()
        self._exercise_level = ExerciseLevel.get_instance()
        self._date_of_practice = DateOfPractice.get_instance()
        self._exercise_modes = ExerciseModes.get_instance()
        self._user_profile = UserProfile.get_instance()
        EditAccInfo()

    def show_window(self):
        self._username.refresh_data()
        self._email.refresh_data()
        self._phone.refresh_data()
        self._gender.set_gender()
        self._date_of_birth.set_age()
        self._height.set_height()
        self._weight.set_weight()
        self._activity_level.set_activity()
        self._exercise_level.set_exercise()
        self._date_of_practice.set_date_of_practice()
        self._exercise_modes.set_exercise_mode()
        self._ui.Acc_warning_lbl.setText("")
        self._ui.Log_out_btn.show()
        self._ui.Menu_screen.show()

    def hide_window(self):
        self._ui.Menu_screen.hide()

    def save_profile(self):
        self._user_profile.save_profile()

    def log_out(self):
        """Log out user from the app"""
        LogOutWindow(self._ui.Menu_screen).show()

    def back_function(self):
        """Send back to previous screen"""
        element = self._controller.contain_in_list(start_window.StartWindow.__name__)
        if element is not None:
            self._controller.select_window(element)
        else:
            self._controller.select_window(start_window.StartWindow())







